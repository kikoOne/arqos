package servicesTienda;

import java.util.HashMap;
import java.util.StringTokenizer;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author J.Guzmán
 */
@WebService(serviceName = "AnalisisTexto")
public class AnalisisTexto {

    @WebMethod(operationName = "numero_caracteres")
    public int numero_caracteres(@WebParam(name = "x") String x) throws ExceptionConversion {
        int p = 0;
        char[] arr = x.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            p++; //conotando espacios como caracteres
        }
        if (p > 200) {
            ExceptionConversion ex = new ExceptionConversion(new ExceptionConversionBean("Excedido número de caracteres"));
            throw ex;
        } else {
            return p;
        }
    }
}
