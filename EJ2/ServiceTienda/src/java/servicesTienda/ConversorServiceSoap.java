/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicesTienda;

/**
 *
 * @author jguzman
 */
public class ConversorServiceSoap {

    public static double conversionRate(conv.Currency fromCurrency, conv.Currency toCurrency) {
        conv.CurrencyConvertor service = new conv.CurrencyConvertor();
        conv.CurrencyConvertorSoap port = service.getCurrencyConvertorSoap();
        return port.conversionRate(fromCurrency, toCurrency);
    }
}
