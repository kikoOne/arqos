package servicesTienda;

import javax.xml.ws.WebFault;

/**
 *
 * @author J.Guzmán
 */
@WebFault(name = "ErrorPorCero")
public class ExceptionConversion extends Exception {

    private ExceptionConversionBean errorBean = null;

    public ExceptionConversion(ExceptionConversionBean errorBean) {
        super(errorBean.getMessage());
        this.errorBean = errorBean;
    }

    public ExceptionConversion(ExceptionConversionBean errorBean, String message, Throwable cause) {
        super(message, cause);
        this.errorBean = errorBean;
    }

    public ExceptionConversion(String message) {
        super(message);
    }

    public ExceptionConversionBean getErrorBean() {
        return errorBean;
    }
}
