/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicesTienda;

import java.io.Serializable;

/**
 *
 * @author J.Guzmán
 */
public class Cd extends Product implements Serializable{

    private String author;
    private String country;
    private String barCode;

    public Cd(String author, String country, String barCode, String id, String title, String description, Float price, Float vat, String year, String type, Integer stock) {
        super(id, title, description, price, vat, year, type, stock);
        this.author = author;
        this.country = country;
        this.barCode = barCode;
    }

    public Cd() {
        super();
    }

    public String getAuthor() {
        return author;
    }

    public String getBarCode() {
        return barCode;
    }

    public String getCountry() {
        return country;
    }

    public String getCode() {
        return barCode;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }
}
