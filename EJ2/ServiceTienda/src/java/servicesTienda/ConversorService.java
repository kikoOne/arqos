package servicesTienda;

import conv.Currency;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author jguzman
 */
@WebService(serviceName = "ConversorService")
public class ConversorService {

    /**
     * This is a sample web service operation
     *
     * @param products
     * @param to
     * @return
     * @throws servicesTienda.ExceptionConversion
     */
    @WebMethod(operationName = "convertCurrency")
    public ArrayList<Cd> convertCurrency(@WebParam(name = "products") ArrayList<Cd> products, @WebParam(name = "to") String to) throws ExceptionConversion {

        float rate;
        switch (to) { // A:
            case "EUR": //EUROS
                rate = (float) ConversorServiceSoap.conversionRate(Currency.USD, Currency.EUR);
                break;
            case "JPY": //YEN JAPONES
                rate = (float) ConversorServiceSoap.conversionRate(Currency.USD, Currency.JPY);
                break;
            case "CNY": //YUAN CHINO
                rate = (float) ConversorServiceSoap.conversionRate(Currency.USD, Currency.CNY);
                break;
            case "RUB": //RUBLOS RUSOS
                rate = (float) ConversorServiceSoap.conversionRate(Currency.USD, Currency.RUB);
                break;
            case "USD":
                rate = 1;
                break;
            default:
                ExceptionConversion err = new ExceptionConversion(new ExceptionConversionBean("Moneda de destino no aceptada"));
                throw err;
        }

//        ArrayList<Cd> cds = new ArrayList<>();
        for (Cd product : products) {
            product.setPrice(product.getPrice() * rate);
//            cds.add(new Cd(((Cd)product).getAuthor(), ((Cd)product).getCountry(), ((Cd)product).getBarCode(), product.getId(), product.getTitle(), 
//                                                        product.getDescription(), product.getPrice(), product.getVat(), product.getYear(), product.getType(), product.getStock()));
        }
        
        return products;
    }
}
