
package conv;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the conv package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConvertCurrency_QNAME = new QName("http://servicesTienda/", "convertCurrency");
    private final static QName _ErrorPorCero_QNAME = new QName("http://servicesTienda/", "ErrorPorCero");
    private final static QName _ConvertCurrencyResponse_QNAME = new QName("http://servicesTienda/", "convertCurrencyResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: conv
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConvertCurrencyResponse }
     * 
     */
    public ConvertCurrencyResponse createConvertCurrencyResponse() {
        return new ConvertCurrencyResponse();
    }

    /**
     * Create an instance of {@link ExceptionConversion }
     * 
     */
    public ExceptionConversion createExceptionConversion() {
        return new ExceptionConversion();
    }

    /**
     * Create an instance of {@link ConvertCurrency }
     * 
     */
    public ConvertCurrency createConvertCurrency() {
        return new ConvertCurrency();
    }

    /**
     * Create an instance of {@link ExceptionConversionBean }
     * 
     */
    public ExceptionConversionBean createExceptionConversionBean() {
        return new ExceptionConversionBean();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertCurrency }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicesTienda/", name = "convertCurrency")
    public JAXBElement<ConvertCurrency> createConvertCurrency(ConvertCurrency value) {
        return new JAXBElement<ConvertCurrency>(_ConvertCurrency_QNAME, ConvertCurrency.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExceptionConversion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicesTienda/", name = "ErrorPorCero")
    public JAXBElement<ExceptionConversion> createErrorPorCero(ExceptionConversion value) {
        return new JAXBElement<ExceptionConversion>(_ErrorPorCero_QNAME, ExceptionConversion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertCurrencyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicesTienda/", name = "convertCurrencyResponse")
    public JAXBElement<ConvertCurrencyResponse> createConvertCurrencyResponse(ConvertCurrencyResponse value) {
        return new JAXBElement<ConvertCurrencyResponse>(_ConvertCurrencyResponse_QNAME, ConvertCurrencyResponse.class, null, value);
    }

}
