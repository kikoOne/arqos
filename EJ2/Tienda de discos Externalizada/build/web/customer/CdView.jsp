<%@include file="../frame/top.jsp" %>
<title>${cd.title}</title>
</head>
<body>
    <%@include file="../frame/headerCustomer.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>${cd.title}</h1>
        </header>

        <c:set var="boton" scope="page" value="A�adir al carrito"/>
        <c:set var="disabled" scope="page" value=""/>
        <c:if test="${cd.stock == 0}">
            <c:set var="disabled" scope="page" value="disabled"/>
            <c:set var="boton" scope="page" value="No disponible"/>
        </c:if>
        <object data="./images/products/${cd.id}.jpg" type="image/png" height="200">
            <img src="./images/products/none.jpg" height="200">
        </object>    
        <h3>Descripci�n</h3>
        <p>${cd.description}</p>
        <h3>Logitud descripci�n: ${description_length}</h3>
        <h3>Autor: ${cd.author}</h3>
        <h3>A�o: ${cd.year}</h3>
        <h3>Pa�s: ${cd.country}</h3>
        <c:if test="${not empty cd.barCode}">
            <h3>Identificador: ${cd.barCode}</h3>
        </c:if>

        <h3>Precio: 
            <fmt:formatNumber type="number" maxFractionDigits="2" value="${cd.price * 1.21 * rate}"/> ${currencyName}</h3>

        <form action="" method="post">
            <select id="currency" name="currency" onchange="this.form.submit()">
                <option value="USD">Selecciona una moneda</option>
                <option value="USD">USD</option>
                <option value="EUR" >EUR</option>
                <option value="JPY">JPY</option>
                <option value="CNY">CNY</option>
                <option value="RUB">RUB</option>
            </select>
            <c:if test="${errorRate != null}">
                ${errorRate}
            </c:if>
        </form>
        <br>
        <br>
        Existen ${cd.stock} unidades disponibles.
        <audio src="./previews/${cd.id}.ogg" autoplay></audio>
        <br>
        <br>
        <form class="pure-form" action="store?location=addCart" method="post" accept-charset="UTF-8">
            <input name="quantity" type="number" value="1" min="1" max="${cd.stock}" ${disabled}>
            <button name="product" value="${cd.id}" class="pure-button pure-button-primary" type="submit" ${disabled}>${boton}</button>
        </form>

        <br>
        <br>
        <c:choose>
            <c:when test="${empty valorations}">
                <p>No hay valoraciones para este art�culo</p>
            </c:when>
            <c:otherwise>
                <table>
                    <c:forEach var="valoration" items="${valorations}">
                        <tr>          
                            <td>${valoration.userEmail}</td>
                            <td width="100">
                                <c:forEach var="i" begin="1" end="${valoration.score}">
                                    <img src="./images/star.jpg" height="15">
                                </c:forEach>
                            </td>
                            <td>${valoration.comment}</td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>

        <h3>Escribe tu valoraci�n sobre el art�culo</h3>
        <form id="form" class="pure-form" action="store?location=addValoration" method="post" accept-charset="UTF-8" autocomplete="off">
            <input type="hidden" name="id" value="${cd.id}">
            <fieldset>
                <textarea rows="5" cols="40" form="form" name="comment"></textarea>
            </fieldset>   
            Puntuaci�n<br>
            <fieldset>
                <input required type="number" name="score" placeholder="5" max="5" min="1">
            </fieldset>  
            <button class="pure-button pure-button-primary"type="submit">Enviar valoraci�n</button>
        </form>
    </section>     
</body>
<%@include file="../frame/bottomCustomer.jsp"%>