<%@include file="../frame/top.jsp" %>
<title>Confirmación de compra</title>
</head>
<body>
    <%@include file="../frame/headerCustomer.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Confirmación de compra<br>
                <c:choose>
                    <c:when test="${customer.expenditure >= 100}">
                        <fmt:formatNumber type="number" maxFractionDigits="2" value="${cart.total * 0.8 * 1.21 * rate}"/> ${currencyName}</h1>
                    </c:when>
                    <c:otherwise>
                        <fmt:formatNumber type="number" maxFractionDigits="2" value="${cart.total * 1.21 * rate}"/> ${currencyName}</h1>
                </c:otherwise>
            </c:choose>
        </header>
        <c:choose>
            <c:when test="${empty cart.products}">
                No hay productos en el carrito.
            </c:when>
            <c:otherwise>                   
                <div style="margin-top: 50px">
                </div>
                <div style="margin-top: 25px">
                    <form class="pure-form"  id="form" action="store?location=confirmPurchase" method="post" accept-charset="UTF-8">
                        <h3>Confirme los datos de envío:</h3>
                        Nombre y apellidos:<br><input type="text" value="${customer.name}" disabled><br><br>
                        Correo electrónico:<br><input type="text" value="${customer.email}" disabled><br><br>
                        Dirección:<br>
                        <textarea form="form" name="shipAddress" cols="45" rows="5">${customer.address}</textarea><br><br>
                        <button class="pure-button pure-button-primary" type="submit">Finalizar compra</button>
                    </form> 
                </div>             
            </c:otherwise>
        </c:choose>
    </section>     
</body>
<%@include file="../frame/bottomCustomer.jsp"%>