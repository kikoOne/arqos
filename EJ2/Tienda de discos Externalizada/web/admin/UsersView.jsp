<%@include file="../frame/top.jsp" %>
<title>Cat�logo</title>
</head>
<body>
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Clientes</h1>
        </header>
        <c:if test="${empty customers}">
            No hay usuarios en la base de datos.
        </c:if>
        <table>
            <c:forEach var="customer" items="${customers}">
                <tr>
                    <td>${customer.name}</td>
                    <td>${customer.email}</td>
                    <td><button class="pure-button pure-button-primary" onclick="location.href = 'admin?location=removeUser&email=${customer.email}'">x</button></td>
                    <td><button class="pure-button pure-button-primary" onclick="location.href = 'admin?location=gotoPasswordChange&email=${customer.email}'">Modificar contrase�a</button></td>
                </tr>
            </c:forEach>
        </table> 
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>