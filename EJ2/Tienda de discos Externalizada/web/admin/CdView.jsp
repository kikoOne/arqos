<%@include file="../frame/top.jsp" %>
<title>${cd.title}</title>
</head>
<body>
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>${cd.title}</h1>
        </header>

        <c:set var="boton" scope="page" value="A�adir al carrito"/>
        <c:set var="disabled" scope="page" value=""/>
        <c:if test="${cd.stock == 0}">
            <c:set var="disabled" scope="page" value="disabled"/>
            <c:set var="boton" scope="page" value="No disponible"/>
        </c:if>
        <object data="./images/products/${cd.id}.jpg" type="image/png" height="200">
            <img src="./images/products/none.jpg" height="200">
        </object>    
        <h3>Descripci�n</h3>
        <p>${cd.description}</p>

        <h3>Autor: ${cd.author}</h3>
        <h3>A�o: ${cd.year}</h3>
        <h3>Pa�s: ${cd.country}</h3>
        <c:if test="${not empty cd.barCode}">
            <h3>Identificador: ${cd.barCode}</h3>
        </c:if>

        <h3>Precio: 
            <fmt:formatNumber type="number" maxFractionDigits="2" value="${cd.price * 1.21}"/>$</h3>
        <br>
        Existen ${cd.stock} unidades disponibles.
        <br>
        <br>
        <c:choose>
            <c:when test="${empty valorations}">
                <p>No hay valoraciones para este art�culo</p>
            </c:when>
            <c:otherwise>
                <table>
                    <c:forEach var="valoration" items="${valorations}">
                        <tr>          
                            <td>${valoration.userEmail}</td>
                            <td width="100">
                                <c:forEach var="i" begin="1" end="${valoration.score}">
                                    <img src="./images/star.jpg" height="15">
                                </c:forEach>
                            </td>
                            <td>${valoration.comment}</td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>