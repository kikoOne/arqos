<%@include file="../frame/top.jsp"%>
<title>Error</title>
</head>
<body>
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Ha ocurrido un error</h1>
        </header>
        <c:if test="${not empty error}">
            <fieldset style="color: red">
                ${error}
            </fieldset>
        </c:if>
    </section>  
</body>
<%@include file="../frame/bottomAdmin.jsp"%>