<header>
    <a href="admin"><img id="logo" src="images/logo.png" height="50" alt="Logo"></a>
    <nav id="navp">
        <a href="admin">Cat�logo</a>
        <a href="admin?location=gotoNewProduct">A�adir producto</a>
        <a href="admin?location=gotoUsers">Gestionar usuarios</a>
        <c:choose>
            <c:when test="${empty admin}"> 
                <a id="ultimoElemento" href="admin?location=gotoLogin">Iniciar sesi�n</a>
            </c:when>
            <c:otherwise>   
                <a id="ultimoElemento" href="admin?location=logout">Cerrar sesi�n</a>
            </c:otherwise>
        </c:choose>
    </nav>      
</header>
