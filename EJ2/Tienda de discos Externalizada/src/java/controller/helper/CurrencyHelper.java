/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.helper;

import conv.ExceptionConversion_Exception;
import javax.servlet.http.HttpServletRequest;
import model.service.ServicesClient;

/**
 *
 * @author jguzman
 */
public class CurrencyHelper {

    private final HttpServletRequest request;

    public CurrencyHelper(HttpServletRequest request) {
        this.request = request;
    }

    public void checkCurrency() {
        String newCurrency = (String) request.getParameter("currency");
        String currency = (String) request.getSession().getAttribute("currencyName");
        if (newCurrency != null && !newCurrency.equals(currency)) {
            request.getSession().setAttribute("currencyName", newCurrency);
            try {
                float rate = (float) ServicesClient.convertCurrency("USD", newCurrency);
                request.getSession().setAttribute("rate", rate);
            } catch (ExceptionConversion_Exception ex) {
                request.getSession().setAttribute("rate", 1);
                request.getSession().setAttribute("currencyName", "USD");
                request.setAttribute("errorRate", ex);
            }
        } else if (newCurrency == null && currency == null) {
            request.getSession().setAttribute("rate", 1);
            request.getSession().setAttribute("currencyName", "USD");
        }
    }
}
