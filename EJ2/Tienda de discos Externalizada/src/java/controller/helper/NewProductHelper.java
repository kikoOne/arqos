package controller.helper;

import atxt.ExceptionConversion_Exception;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.service.ServicesClient;
import model.vo.Cd;
import model.vo.Product;

public class NewProductHelper {

    private final HttpServletRequest request;
    private final ProductDAO productDao;

    public NewProductHelper(HttpServletRequest request) {
        this.request = request;

        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        productDao = daoFactory.getProductDAO();
    }

    public boolean insertProduct() {
        if (request.getSession().getAttribute("admin") != null) {
            try {
                Product product = null;
                String type = request.getParameter("type");
                if (type.matches("cd")) {
                    Float price;
                    Integer stock, iyear;
                    try {
                        price = Float.parseFloat(request.getParameter("price"));
                        stock = Integer.parseInt(request.getParameter("stock"));
                        iyear = Integer.parseInt(request.getParameter("year"));
                    } catch (NumberFormatException ex) {
                        request.setAttribute("error", "Error en el formato de los datos.");
                        return false;
                    }
                    String description = request.getParameter("description");
                    String year = request.getParameter("year");
                    String title = request.getParameter("title");
                    String author = request.getParameter("author");
                    String country = request.getParameter("country");
                    String barcode = request.getParameter("barcode");
                    
                    boolean flg = false;
                    
                    try {
                        ServicesClient.numeroCaracteres(description);
                    } catch (ExceptionConversion_Exception ex) {
                        request.setAttribute("error", ex.getMessage());
                        return false;
                    }
                    
                    // Comprobación de datos recibidos
                    if (barcode == null || barcode.length() > 12 || barcode.equals("") || country == null || country.equals("") || country.length() > 40 || author == null || author.equals("") || author.length() > 50 || title == null || title.equals("") || title.length() > 50 || description == null || description.equals("") || description.length() > 1000 || iyear > new Date().getYear() || iyear < 1900 || price < 0 || stock < 0) {
                        request.setAttribute("error", "Error en el formato de los datos.");
                        return false;
                    }

                    product = new Cd(author, country, barcode, null, title, description, price, null, year, type, stock);
                }
                if (productDao.addProduct(product)) {
                    request.setAttribute("message", "Producto añadido correctamente.");
                    return true;
                } else {
                    request.setAttribute("error", "Error al introducir el producto en la base de datos.");
                    return false;
                }
            } catch (SQLException ex) {
                request.setAttribute("error", "Error al introducir el producto en la base de datos. Excepcion.");
                return false;
            }
        }
        request.setAttribute("error", "Debe haber iniciado sesion como administrador para realizar esta accion.");
        return false;
    }
}
