package controller.helper;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import model.dao.CustomerDAO;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.vo.Customer;

public class RegisterHelper {

    private final HttpServletRequest request;
    private final CustomerDAO daoCustomer;

    public RegisterHelper(HttpServletRequest request) {
        this.request = request;
        DAOFactoryCreator creator = new DAOFactoryCreator();
        DAOFactory daoFactory = creator.getDAOFactory();
        daoCustomer = daoFactory.getCustomerDAO();
    }

    public boolean addCustomer() {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String address = request.getParameter("shipAddress");

        if (name == null || name.equals("") || name.length() > 200 || email == null || email.equals("") || email.length() > 50 || password == null || password.equals("") || password.length() > 50 || address == null || address.equals("") || address.length() > 1000) {
            request.setAttribute("error", "Error en el formato de los datos.");
            return false;
        }

        Customer customer = new Customer(0.0F, address, name, email, password);
        try {
            daoCustomer.insertCustomer(customer);
            request.setAttribute("message", "Registro correcto.");
            return true;
        } catch (SQLException ex) {
            request.setAttribute("error", "Error al introducir el usuario en la base de datos.");
        }

        request.setAttribute("error", "Error al crear el usuario.");
        return false;
    }
}
