
package model.service;

import conv.ExceptionConversion_Exception;

/**
 *
 * @author jguzman
 */
public class ServicesClient {

    public static double convertCurrency(java.lang.String from, java.lang.String to) throws ExceptionConversion_Exception {
        conv.ConversorService_Service service = new conv.ConversorService_Service();
        conv.ConversorService port = service.getConversorServicePort();
        return port.convertCurrency(from, to);
    }

    public static int numeroCaracteres(java.lang.String x) throws atxt.ExceptionConversion_Exception {
        atxt.AnalisisTexto_Service service = new atxt.AnalisisTexto_Service();
        atxt.AnalisisTexto port = service.getAnalisisTextoPort();
        return port.numeroCaracteres(x);
    }
}
