/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServClient;

import services.ExceptionConversion_Exception;

/**
 *
 * @author jguzman
 */
public class ServicesClient {

    public static double convertCurrency(java.lang.String from, java.lang.String to) throws ExceptionConversion_Exception {
        services.ConversorService_Service service = new services.ConversorService_Service();
        services.ConversorService port = service.getConversorServicePort();
        return port.convertCurrency(from, to);
    }

    public static int numeroCaracteres(java.lang.String x) throws s2.ExceptionConversion_Exception {
        s2.AnalisisTexto_Service service = new s2.AnalisisTexto_Service();
        s2.AnalisisTexto port = service.getAnalisisTextoPort();
        return port.numeroCaracteres(x);
    }
}
