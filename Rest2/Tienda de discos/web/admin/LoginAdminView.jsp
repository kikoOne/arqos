<%@include file="../frame/top.jsp"%>
<title>Acceso Administrador</title>
</head>
<body ng-controller="MyCtrl">
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Acceso de administración</h1>
        </header>
        <br>
        <c:if test="${not empty message}">
            <fieldset style="color: green">
                ${message}
            </fieldset>
        </c:if>
        <form class="pure-form" role="form" ng-submit="login()">
            <fieldset>
                <input required type="email" name="email" placeholder="nombre@ejemplo.com" maxlength="100" style="width: 250px" class="form-control" ng-model="email">
            </fieldset>        
            <fieldset>
                <input required type="password" placeholder="&bull;&bull;&bull;&bull;" name="password" maxlength="100" style="width: 250px" class="form-control" ng-model="password">
            </fieldset>             
            <button class="pure-button pure-button-primary" type="submit">Entrar</button>
        </form>
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>
