<%@include file="../frame/top.jsp" %>
<title>Carrito de la compra</title>
</head>
<body>
    <%@include file="../frame/headerCustomer.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Carrito de la compra</h1>
        </header>

        <c:choose>
            <c:when test="${empty cart.products}">
                No hay art�culos en el carrito.
            </c:when>
            <c:otherwise>
                <table>
                    <tr>
                        <td>T�tulo</td>
                        <td>Imagen</td>
                        <td>Precio/ud.</td>
                        <td>Precio/ud. + IVA</td>
                        <td>Cantidad</td>
                        <td>Precio + IVA</td>
                        <td></td>
                    </tr>

                    <c:forEach var="hashEntry" items="${cart.products}">
                        <c:set var="boton" scope="page" value="A�adir al carrito"/>
                        <c:set var="disabled" scope="page" value=""/>
                        <c:if test="${hashEntry.value.product.stock == 0}">
                            <c:set var="disabled" scope="page" value="disabled"/>
                            <c:set var="boton" scope="page" value="No disponible"/>
                        </c:if>

                        <tr>
                            <td>${hashEntry.value.product.title}</td>
                            <td>
                                <object data="./images/products/${hashEntry.value.product.id}.jpg" type="image/png" height="70">
                                    <img src="./images/products/none.jpg" height="70">
                                </object>
                            </td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${hashEntry.value.product.price}"/>$</td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${hashEntry.value.product.price * hashEntry.value.product.vat}"/>$</td>
                            <td>${hashEntry.value.quantity} uds.</td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${hashEntry.value.product.price * hashEntry.value.product.vat * hashEntry.value.quantity}"/>$</td>
                            <td><button class="pure-button pure-button-primary" onclick="location.href = 'store?location=removeCart&id=${hashEntry.key}'">x</button></td>
                        </tr>
                    </c:forEach>

                </table> 
                <p>TOTAL: <fmt:formatNumber type="number" maxFractionDigits="2" value="${cart.totalVat}"/>$</p>
                <br>
                <c:if test="${customer.expenditure >= 100}">
                    Es cliente VIP. Se aplicar� un descuento del 20%
                    <p>TOTAL A PAGAR: <fmt:formatNumber type="number" maxFractionDigits="2" value="${cart.totalVat * 0.8}"/>$</p>
                </c:if>
                <button class="pure-button pure-button-primary" onclick="location.href = 'store?location=startPurchase'">Comprar</button>  
            </c:otherwise>
        </c:choose>
    </section>     
</body>
<%@include file="../frame/bottomCustomer.jsp"%>