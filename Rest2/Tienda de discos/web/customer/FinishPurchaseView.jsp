<%@include file="../frame/top.jsp" %>
<title>Confirmaci�n de compra</title>
</head>
<body>
    <%@include file="../frame/headerCustomer.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Pasarela de pago</h1>
            IMPORTE TOTAL: 
            <b>
                <c:choose>
                    <c:when test="${customer.expenditure >= 100}">
                        <fmt:formatNumber type="number" maxFractionDigits="2" value="${cart.totalVat * 0.8}"/>$</p>
                    </c:when>
                    <c:otherwise>
                        <fmt:formatNumber type="number" maxFractionDigits="2" value="${cart.totalVat}"/>$
                    </c:otherwise>
                </c:choose>
            </b>
        </header>
        <c:choose>
            <c:when test="${empty cart.products}">
                No hay productos en el carrito.
                <div style="margin-top: 50px">
                    <button onclick="location.href = '.'">Volver a la tienda</button>
                </div>
            </c:when>
            <c:otherwise>                   
                <div style="margin-top: 50px">
                    <div style="margin-top: 50px">
                        <p>${customer.name}, est� a punto de completar su compra con un importe total de 
                            <c:choose>
                                <c:when test="${customer.expenditure >= 100}">
                                    <fmt:formatNumber type="number" maxFractionDigits="2" value="${cart.totalVat * 0.8}"/>$
                                </c:when>
                                <c:otherwise>
                                    <fmt:formatNumber type="number" maxFractionDigits="2" value="${cart.totalVat}"/>$
                                </c:otherwise>
                            </c:choose>
                            .</p>
                        <p>Una vez realice el pago, se le enviar� la factura por correo electr�nico a la direcci�n "<b>${customer.email}</b>".</p>
                        <p>El pedido ser� enviado a la direcci�n: "<b>${shipAddress}</b>".</p>
                    </div>
                    <div style="margin-top: 50px">
                        <button class="pure-button pure-button-primary" onclick="location.href = 'store?location=finishPurchase'">Realizar el pago</button><br><br>
                    </div>
                </div>      
            </c:otherwise>
        </c:choose>
    </section>     
</body>
<%@include file="../frame/bottomCustomer.jsp"%>