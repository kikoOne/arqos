<%@include file="../frame/top.jsp"%>
<title>Registro</title>
</head>
<body>
    <%@include file="../frame/headerCustomer.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Registro de usuarios</h1>
        </header>
        <form class="pure-form" action="store?location=register" method="post" accept-charset="UTF-8" autocomplete="off">
            <fieldset>
                <input required type="text" name="name" placeholder="Nombre y apellidos" maxlength="150" style="width: 300px">
            </fieldset>   
            <fieldset>
                <input required type="text" name="shipAddress" placeholder="Direcci�n" maxlength="150" style="width: 300px">
            </fieldset>  
            <fieldset>
                <input required type="email" name="email" placeholder="nombre@ejemplo.com" maxlength="100" style="width: 300px">
            </fieldset>        
            <fieldset>
                <input required type="password" name="password" placeholder="&bull;&bull;&bull;&bull;" maxlength="100" style="width: 300px">
            </fieldset>             
            <button class="pure-button pure-button-primary"type="submit">Registrarse</button>
        </form>
    </section>     
</body>
<%@include file="../frame/bottomCustomer.jsp"%>