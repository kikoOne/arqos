<%@include file="../frame/top.jsp"%>
<title>Acceso</title>
</head>
<body>
    <%@include file="../frame/headerCustomer.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Acceso</h1>
        </header>
        <c:if test="${not empty message}">
            <fieldset style="color: green">
                ${message}
            </fieldset>
        </c:if>
        <form class="pure-form" action="store?location=login" method="post" accept-charset="UTF-8" autocomplete="off">
            <fieldset>
                <input required type="email" name="email" placeholder="nombre@ejemplo.com" maxlength="100" style="width: 250px">
            </fieldset>        
            <fieldset>
                <input required type="password" placeholder="&bull;&bull;&bull;&bull;" name="password" maxlength="100" style="width: 250px">
            </fieldset>             
            <button class="pure-button pure-button-primary" type="submit">Entrar</button>
        </form>
    </section>     
</body>
<%@include file="../frame/bottomCustomer.jsp"%>