<%@include file="../frame/top.jsp" %>
<title>Confirmaci�n de compra</title>
</head>
<body>
    <%@include file="../frame/headerCustomer.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Gracias por su compra.</h1>
            <p>En breves recibir� un correo con los detalles de su pedido.</p>
        </header>

        <table class="bill">
            <tr>
                <td class="billc">Nombre</td>
                <td class="billc">N�mero de factura</td>
            </tr>
            <tr>
                <td class="billc">${customer.name}</td>
                <td class="billc">${bill.billNumber}</td>
            </tr>
        </table>
        <table class="bill">
            <tr>
                <td class="billc">Direcci�n</td>
            </tr>
            <tr>
                <td class="billc">${bill.shipAddress}</td>
            </tr>
        </table>
        <table class="bill">
            <tr>
                <td class="billc">C�digo</td>
                <td class="billc">Nombre</td>
                <td class="billc">Imagen</td>
                <td class="billc">Unidades</td>
                <td class="billc">Total</td>
                <td class="billc">IVA</td>
                <td class="billc">Total + IVA</td>
            </tr>

            <c:forEach var="product" items="${bill.products}">
                <tr>
                    <td class="billc">${product.product.id}</td>
                    <td class="billc">${product.product.title}</td>
                    <td class="billc">
                        <object data="./images/products/${product.product.id}.jpg" type="image/png" height="70">
                            <img src="./images/products/none.jpg" height="70">
                        </object>
                    </td>                    <td>${product.quantity}</td>
                    <td class="billc"><fmt:formatNumber type="number" maxFractionDigits="2" value="${product.product.price}"/>$</td>
                    <td class="billc">21%</td>
                    <td class="billc"><fmt:formatNumber type="number" maxFractionDigits="2" value="${product.product.price * product.product.vat}"/>$</td>
                </tr>
            </c:forEach>
        </table>

        <br><br>
        <table class="bill">
            <tr>
                <td class="billc">Total</td>
                <td class="billc">Total + IVA</td>
            </tr>
            <tr>
                <td class="billc">
                    <fmt:formatNumber type="number" maxFractionDigits="2" value="${bill.totalAmount}"/>$
                </td>
                <td class="billc">
                    <fmt:formatNumber type="number" maxFractionDigits="2" value="${bill.totalAmountVat}"/>$
                </td>
            </tr>
        </table>      
    </section>     
</body>
<%@include file="../frame/bottomCustomer.jsp"%>