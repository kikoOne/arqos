<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page isELIgnored="false"%>
<%--c:if test="${empty server}">
    <jsp:forward page="/forbidden.jsp"/>
</c:if--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang = "es" ng-app="MyApp">
    <head>
        <meta charset="UTF-8">
        <link href="css/pure-min.css" rel="stylesheet">
        <link href="css/estilos.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/scroll.js"></script>   
        <script type="text/javascript" src="js/libs/angular.js/angular.js" ></script>
        <script type="text/javascript" src="js/libs/angular.js/angular-resource.js" ></script>
        <script type="text/javascript" src="js/servicios.js"></script>