<footer>
    Tienda de discos &copy; 2015 - <a href="store">Zona de clientes</a>
    <br>
    <br>
    <c:choose>
        <c:when test="${empty admin}">
            <a href="admin?location=gotoLogin">Iniciar sesi�n</a>
        </c:when>
        <c:otherwise>   
            ${admin.email} | <a href="admin?location=logout">Cerrar sesi�n</a>
        </c:otherwise>
    </c:choose>
</footer>
</html>