package controller.helper;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.service.Cart;
import model.vo.Product;

public class CartHelper {

    private final HttpServletRequest request;

    public CartHelper(HttpServletRequest request) {
        this.request = request;
    }

    public Boolean addCart() {

        Cart cart;
        if (logged()) { // Si esta logueado el usuario
            if (request.getSession().getAttribute("cart") == null) { // Si no tiene un carrito
                cart = new Cart();
                request.getSession().setAttribute("cart", cart);
            }

            cart = (Cart) request.getSession().getAttribute("cart");

            DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
            DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
            ProductDAO productDAO = daoFactory.getProductDAO();

            Integer quantity = Integer.parseInt(request.getParameter("quantity"));
            String id = request.getParameter("product");

            Product p;
            try {
                p = productDAO.getProduct(id);
            } catch (SQLException ex) {
                request.setAttribute("error", "Error en el acceso a la base de datos.");
                return false;
            }

            if (p == null) {
                request.setAttribute("error", "El producto indicado no existe.");
                return false;
            }
            // Si el producto ya está en el carrito
            if (cart.getProducts().containsKey(p.getId())) {
                // Si el stock disponible permite añadir lo requerido
                if (p.getStock() >= quantity + cart.getProducts().get(p.getId()).getQuantity()) {
                    cart.addProduct(p, quantity + cart.getProducts().get(p.getId()).getQuantity());
                } else {
                    // Se actualiza el stock al máximo y se envía mensaje de error
                    if (p.getStock() != 0) {
                        cart.addProduct(p, p.getStock());
                        request.setAttribute("error", "No queda suficiente stock. El stock se ha actualizado al máximo posible.");
                    } else {
                        cart.removeProduct(p.getId());
                        request.setAttribute("error", "No queda stock.");

                    }
                    return false;
                }
                return true;
            } else {
                // Si el stock disponible permite añadir lo requerido
                if (p.getStock() >= quantity) {
                    cart.addProduct(p, quantity);
                } else {
                    // Se actualiza el stock al máximo y se envía mensaje de error
                    if (p.getStock() != 0) {
                        cart.addProduct(p, p.getStock());
                        request.setAttribute("error", "No queda suficiente stock. El stock se ha actualizado al máximo posible.");
                    } else {
                        cart.removeProduct(p.getId());
                        request.setAttribute("error", "No queda stock.");
                    }
                    return false;
                }
                return true;
            }
        }
        request.setAttribute("error", "Debe iniciar sesión para añadir productos al carrito.");

        return false;
    }

    public Boolean removeCart() {
        if (request.getSession(false) != null) { // Si hay una sesión
            if (request.getSession().getAttribute("cart") != null) { // Si tiene un carrito
                String id = request.getParameter("id");
                Cart cart = (Cart) request.getSession().getAttribute("cart");
                cart.removeProduct(id);
            }
            return true;
        }
        return false;
    }

    // Si hay una sesión  
    public Boolean logged() {
        if (this.request.getSession().getAttribute("customer") == null) {
            request.setAttribute("error", "Debes iniciar sesión para utilizar el carrito.");
            return false;
        }
        return true;
    }

    public Boolean resetCart() {
        if (request.getSession(false) != null) { // Si hay una sesión
            if (request.getSession().getAttribute("cart") != null) { // Si tiene un carrito
                request.getSession().setAttribute("cart", null);
            }
            return true;
        }
        return false;
    }
}
