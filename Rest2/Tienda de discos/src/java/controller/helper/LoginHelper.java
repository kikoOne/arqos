package controller.helper;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import model.dao.CustomerDAO;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.vo.Customer;

public class LoginHelper {

    private final HttpServletRequest request;
    private final CustomerDAO cDAO;
    private final String email;
    private final String password;
    private Customer customer;

    public LoginHelper(HttpServletRequest request) {
        this.email = request.getParameter("email");
        this.password = request.getParameter("password");
        this.request = request;

        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        cDAO = daoFactory.getCustomerDAO();
    }

    public boolean logout() {
        if (request.getSession(false) != null) { // Si hay una sesión
            if (request.getSession(false).getAttribute("customer") != null) {
                request.getSession().invalidate();
                return true;
            }
        }
        request.setAttribute("error", "No hay una sesión abierta.");
        return false;
    }

    public boolean login() {
        // Si ya hay una sesión vuelve al catálogo
        if (request.getSession(false) != null) { // Si hay una sesión
            if (request.getSession(false).getAttribute("customer") != null) {
                return true;
            }
        }
        try {
            customer = cDAO.getCustomer(email, password);
            if (customer != null) {
                request.getSession(true).setAttribute("customer", customer);
                return true;
            } else {
                request.setAttribute("error", "Usuario o contraseña incorrectos.");
                return false;
            }
        } catch (SQLException ex) {
            request.setAttribute("error", "Error en el acceso a la base de datos.");
            return false;
        }
    }
}
