package controller.helper;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.vo.Cd;
import model.vo.Product;

public class CatalogHelper {

    private final ProductDAO pDAO;
    private ArrayList<Product> list;
    private final HttpServletRequest request;

    public CatalogHelper(HttpServletRequest request) {
        this.request = request;
        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        pDAO = daoFactory.getProductDAO();
    }

    public String getType() {
        String type = null;
        try {
            type = pDAO.getType(request.getParameter("id"));
        } catch (SQLException ex) {
            request.setAttribute("error", "Error en el acceso a la base de datos.");
        }
        return type;
    }

    public boolean getCd() {
        try {
            Cd cd = pDAO.getCd(request.getParameter("id"));
            this.request.setAttribute("cd", cd);
            return true;
        } catch (SQLException ex) {
            request.setAttribute("error", "Error en el acceso a la base de datos.");
            return false;
        }
    }

    public boolean getCatalog() {
        try {
            list = pDAO.getList(null);
        } catch (SQLException ex) {
            request.setAttribute("error", "Error en el acceso a la base de datos.");
            return false;
        }
        if (list == null) {
            request.setAttribute("error", "No hay ningún artículo.");
            return false;
        }
        request.setAttribute("products", list);
        return true;
    }

    public short getCatalogAdmin() {
        if (request.getSession().getAttribute("admin") != null) {
            try {
                list = pDAO.getList(null);
            } catch (SQLException ex) {
                request.setAttribute("error", "Error en el acceso a la base de datos.");
                return 0;
            }
            if (list == null) {
                request.setAttribute("error", "No hay ningún artículo.");
                return 0;
            }
            request.setAttribute("products", list);
            return 1;
        } else {
            request.setAttribute("error", "Error. Debe estar logueado como usuario.");
            return -1;
        }
    }
}
