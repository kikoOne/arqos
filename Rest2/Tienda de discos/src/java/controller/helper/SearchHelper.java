package controller.helper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.vo.Product;

public class SearchHelper {

    private final ProductDAO pDAO;
    private ArrayList<Product> list;
    private final HttpServletRequest request;
    private final HashMap<String, String> filter;

    public SearchHelper(HttpServletRequest request) {
        this.request = request;
        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        pDAO = daoFactory.getProductDAO();
        this.filter = new HashMap<>();
    }

    public boolean getFilteredList() {

        Enumeration<String> parameters = request.getParameterNames();

        while (parameters.hasMoreElements()) {
            String parameter = parameters.nextElement();
            this.filter.put(parameter, request.getParameter(parameter));
        }

        try {
            list = pDAO.getList(this.filter);
        } catch (SQLException ex) {
            request.setAttribute("error", "Error en el acceso a la base de datos.");
            return false;
        }
        if (list == null) {
            request.setAttribute("error", "No hay ningún artículo.");
            return false;
        }
        request.setAttribute("products", list);
        return true;
    }
}
