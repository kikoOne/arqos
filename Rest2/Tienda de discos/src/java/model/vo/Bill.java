/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.vo;

import java.util.ArrayList;

/**
 *
 * @author J.Guzmán
 */
public class Bill {

    private ArrayList<CartProduct> products;
    private String shipAddress;
    private Float totalAmount;
    private Float totalAmountVat;
    private Float discount;
    private Integer billNumber;

    public Integer getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(Integer billNumber) {
        this.billNumber = billNumber;
    }


    public Bill() {
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public ArrayList<CartProduct> getProducts() {
        return products;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public Float getTotalAmount() {
        return totalAmount;
    }

    public Float getTotalAmountVat() {
        return totalAmountVat;
    }

    public void setProducts(ArrayList<CartProduct> products) {
        this.products = products;
    }

    public void setTotalAmountVat(Float totalAmountVat) {
        this.totalAmountVat = totalAmountVat;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public void setTotalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
    }
}
