package model.dao;

public interface DAOFactory {

    public CustomerDAO getCustomerDAO();

    public AdminUserDAO getAdminUserDAO();

    public ProductDAO getProductDAO();

    public ValorationsDAO getValorationsDAO();
}
