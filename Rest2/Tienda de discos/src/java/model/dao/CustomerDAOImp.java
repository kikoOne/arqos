package model.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.vo.Customer;

public class CustomerDAOImp implements CustomerDAO {

    @Override
    public Customer getCustomer(String email) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Customer customer;
            customer = null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM customers WHERE email='" + email + "'");
            if (rs.next()) {
                customer = new Customer(rs.getFloat("expenditure"), rs.getString("address"), rs.getString("name"), rs.getString("email"), rs.getString("password"));
            }
            con.close();
            return customer;
        }
    }

    @Override
    public Customer getCustomer(String email, String password) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Customer customer;
            customer = null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM customers WHERE email='" + email + "' AND password='" + password + "'");
            if (rs.next()) {
                customer = new Customer(rs.getFloat("expenditure"), rs.getString("address"), rs.getString("name"), rs.getString("email"), rs.getString("password"));
            }
            con.close();
            return customer;
        }
    }

    @Override
    public void insertCustomer(Customer customer) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            try {
                con.setTransactionIsolation(4);
                Statement st = con.createStatement();
                st.executeUpdate("INSERT INTO customers (email,password,name,address) VALUES ('" + customer.getEmail() + "','" + customer.getPassword() + "','" + customer.getName() + "','" + customer.getAddress() + "')");
                con.close();
            } catch (SQLException ex) {
                con.rollback();
                con.close();
                throw new SQLException();
            }
        }
    }

    @Override
    public boolean hasPurchasedProduct(String mail, String id) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            ResultSet rs;
            Statement st = con.createStatement();
            rs = st.executeQuery("SELECT * \n"
                    + "FROM purchases \n"
                    + "LEFT JOIN orderLines ON orderLines.purchase=purchases.id \n"
                    + "WHERE purchases.customer='" + mail + "' AND orderLines.id=" + id + ";");
            Boolean brs = rs.next();
            con.close();
            return brs;
        }
    }
}
