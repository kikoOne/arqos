
/**
 *
 * @author jguzman
 */
public class LoginUser {

    private String id;
    private String email;

    public LoginUser() {
    }
    
    public LoginUser(String user, String password) {
        this.id = user;
        this.email = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
