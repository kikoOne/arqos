﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

CREATE DATABASE IF NOT EXISTS `store` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `store`;

CREATE TABLE IF NOT EXISTS `administrators` (
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `administrators` (`email`, `password`, `name`) VALUES
('admin@gmail.com', '1234', 'Administrador');

CREATE TABLE IF NOT EXISTS `cds` (
  `id` bigint(20) NOT NULL,
  `barcode` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `stock` int(11) NOT NULL,
  `year` char(4) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `cds` (`id`, `barcode`, `title`, `author`, `country`, `description`, `price`, `stock`, `year`) VALUES
(1, '888750348623', 'Rock or Bust', 'AC/DC', 'Estados Unidos', 'Rock or Bust es el primer álbum de AC/DC en los 41 años de historia de la banda en el que no participa uno de sus miembros fundadores, Malcolm Young. A principios de este año, AC/DC publicó un comunicado en el que explicaba que debido a una enfermedad, Malcolm se tomaría un descanso. Desafortunadamente, Malcolm no podrá volver a tocar con la banda. AC/DC saldrá de gira para presentar Rock or Bust en 2015. Stevie Young, sobrino de los miembros fundadores Angus y Malcolm Young, es quién se ha encargado de tocar la guitarra rítmica en Rock or Bust y también será quién acompañe a la banda en esta gira mundial.', 11.67, 0, '2014'),
(2, '602537042791', 'Overexposed (Deluxe Edt.)', 'Maroon 5', 'Estados Unidos', 'Overexposed, en español: Sobreexpuesto, es el cuarto álbum de estudio de la banda estadounidense Maroon 5, lanzado por la discográfica A&M/Octone Records en junio de 2012.', 22.44, 51, '2012'),
(3, '888430536623', 'Xscape', 'Michael Jackson', 'Estados Unidos', 'Xscape es el segundo álbum póstumo de estudio de Michael Jackson. Salió a la venta el martes 13 de mayo de 2014, con ocho nuevas canciones preseleccionadas. Más adelante empezó a comercializarse por Europa, y después por el resto del mundo. El álbum póstumo -Xscape-, de Michael Jackson -El Rey del Pop-, se convirtió en Disco de Oro en México, por más de 30 mil copias vendidas después de su estreno en mayo.', 8.99, 18, '2014'),
(4, '602527779089', 'Nevermind (Remaster)', 'Nirvana', 'Reino Unido', 'Nevermind es el segundo álbum de estudio de la banda estadounidense Nirvana, publicado el 24 de septiembre de 1991. Producido por Butch Vig, Nevermind fue el primer lanzamiento de la banda con DGC Records. El líder de la agrupación, Kurt Cobain, trató de hacer música fuera de los límites restrictivos de la escena grunge de Seattle, aprovechando la influencia de grupos como los Pixies y su uso de la dinámica de canciones ruidosas y calmadas.', 6.76, 59, '1991'),
(5, '509974674812', '1916', 'Motörhead', 'Reino Unido', '1916 es el noveno álbum de estudio de la banda de heavy metal británica Motörhead. Fue lanzado al mercado el 26 de febrero de 1991 por la discográfica WTG Records. Fue su primer álbum con WTG después de su batalla legal con GWR Records. Dicho álbum está un poco más orientado al Hard rock sin perder el toque Heavy de Motorhead, además de poseer una canción con orientación al Punk llamada R.A.M.O.N.E.S. en homenaje a la banda de Punk del mismo nombre.', 8.99, 40, '1991'),
(6, NULL, 'Javier Guzmán Figueira y Domínguez', 'Javier Guzmán Figueira y Domínguez', 'España', 'Javier Guzmán recibió el Segundo Premio a la Excelencia Musical en 2010, otorgado por tres instituciones gallegas: Consellería de Educación e O.U., Fundación Paideia Galiza y Estudio de Grabación MANS. Premio que le permitió grabar su primer disco: "Javier Guzmán Figueira y Domínguez".<br><br><b>BEETHOVEN<br>Sonata No. 9 in A Major, OP.47</b><ul><li>Adagio sostenuto</li><li>Andante con Variazioni</li><li>Finale. Presto</li></ul><b>Maurice Ravel</b><ul><li>Tzigane</ul>', 5.99, 85, '2010');

CREATE TABLE IF NOT EXISTS `customers` (
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `expenditure` float NOT NULL DEFAULT '0',
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `customers` (`email`, `password`, `name`, `expenditure`, `address`) VALUES
('centoloviolino@gmail.com', '1234', 'Javier Guzmán', 7.2479, 'E.T.S.E. Campus Vida, Santiago de Compostela. CP 15782'),
('landesag@gmail.com', '1234', 'César Landesa', 735.332, 'Plaza Roja, Santiago de Compostela. CP 15782'),
('rodrigoemece@gmail.com', '1234', 'Rodrigo Martínez', 407.071, 'Plaza de Vigo, Santiago de Compostela. CP 15782');

CREATE TABLE IF NOT EXISTS `orderLines` (
  `id` bigint(20) NOT NULL,
  `purchase` bigint(20) NOT NULL,
  `partial` float NOT NULL,
  `vat` float NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products` (
`id` bigint(20) NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `products` (`id`, `type`) VALUES
(1, 'cd'),
(2, 'cd'),
(3, 'cd'),
(4, 'cd'),
(5, 'cd'),
(6, 'cd');

CREATE TABLE IF NOT EXISTS `purchases` (
`id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `shipaddress` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `total` float NOT NULL,
  `totalvat` float NOT NULL,
  `customer` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `discount` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `valorations` (
  `id` bigint(20) NOT NULL,
  `customer` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  `comment` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `vats` (
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `vat` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `vats` (`type`, `vat`) VALUES
('cd', 1.21);


ALTER TABLE `administrators`
 ADD PRIMARY KEY (`email`);

ALTER TABLE `cds`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `barcode` (`barcode`);

ALTER TABLE `customers`
 ADD PRIMARY KEY (`email`);

ALTER TABLE `orderLines`
 ADD PRIMARY KEY (`purchase`,`id`), ADD KEY `id` (`id`);

ALTER TABLE `products`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `purchases`
 ADD PRIMARY KEY (`id`), ADD KEY `customer` (`customer`);

ALTER TABLE `valorations`
 ADD PRIMARY KEY (`id`,`customer`), ADD KEY `customer` (`customer`);

ALTER TABLE `vats`
 ADD PRIMARY KEY (`type`);


ALTER TABLE `products`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
ALTER TABLE `purchases`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `cds`
ADD CONSTRAINT `cds_ibfk_1` FOREIGN KEY (`id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `orderLines`
ADD CONSTRAINT `orderLines_ibfk_1` FOREIGN KEY (`id`) REFERENCES `products` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `orderLines_ibfk_2` FOREIGN KEY (`purchase`) REFERENCES `purchases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `purchases`
ADD CONSTRAINT `purchases_ibfk_1` FOREIGN KEY (`customer`) REFERENCES `customers` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `valorations`
ADD CONSTRAINT `valorations_ibfk_1` FOREIGN KEY (`id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `valorations_ibfk_2` FOREIGN KEY (`customer`) REFERENCES `customers` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;
