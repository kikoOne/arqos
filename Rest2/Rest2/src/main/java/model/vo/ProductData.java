/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.vo;

import java.util.ArrayList;

/**
 *
 * @author jguzman
 */
public class ProductData {
    private Cd product;
    private ArrayList<Valoration> valorations;

    public ProductData() {
    }

    public ProductData(Cd product, ArrayList<Valoration> valorations) {
        this.product = product;
        this.valorations = valorations;
    }

    public Cd getProduct() {
        return product;
    }

    public void setProduct(Cd product) {
        this.product = product;
    }

    public ArrayList<Valoration> getValorations() {
        return valorations;
    }

    public void setValorations(ArrayList<Valoration> valorations) {
        this.valorations = valorations;
    }
    
}
