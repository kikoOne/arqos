package model.vo;


/**
 *
 * @author jguzman
 */
public class LoginUser {

    private String password;
    private String email;

    public LoginUser() {
    }
    
    public LoginUser(String email, String password) {
        this.password = email;
        this.email = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String id) {
        this.password = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
