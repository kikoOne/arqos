package es.usc.grei.aos.ejercicio1.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class WSResult {

    public boolean success = false;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Object content = null;

    public WSResult(boolean success) {
        this.success = success;
    }

    public WSResult(boolean success, Object content) {
        this.success = success;
        this.content = content;
    }
}
