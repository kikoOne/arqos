package es.usc.grei.aos.ejercicio1.ws;

import es.usc.grei.aos.ejercicio1.model.AdminUserDAO;
import es.usc.grei.aos.ejercicio1.model.AdminUserDAOImp;
import es.usc.grei.aos.ejercicio1.model.CustomerDAO;
import es.usc.grei.aos.ejercicio1.model.CustomerDAOImp;
import es.usc.grei.aos.ejercicio1.model.LoginUser;
import es.usc.grei.aos.ejercicio1.model.MySQLConnection;
import model.vo.Customer;
import es.usc.grei.aos.ejercicio1.model.WSResult;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.vo.UserData;

/**
 * REST Web Service
 *
 * @author jguzman
 */
@Path("/")
public class UserResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserResource
     */
    public UserResource() {
    }

    @GET
    @Path("users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers() {
        AdminUserDAO adao = new AdminUserDAOImp();
        ArrayList<Customer> customers;
        try {
            customers = adao.getCustomers();
            return Response.ok(new WSResult(true, customers)).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("id") String id) throws SQLException {
        AdminUserDAO adao = new AdminUserDAOImp();
        ArrayList<Customer> customers;
        try {
            customers = adao.getCustomers();
            for (int i = 0; i < customers.size(); i++) {
                if (customers.get(i).getEmail().equals(id)) {
                    return Response.ok(new WSResult(true, customers.get(i))).build();
                }
            }
            return Response.ok(new WSResult(true, customers)).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path("user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(UserData user) {
        CustomerDAO cdao = new CustomerDAOImp();
        Customer customer = new Customer(0.0f, user.getShipAdress(), user.getName(), user.getEmail(), user.getPassword());
        try {
            cdao.insertCustomer(customer);
            return Response.status(Response.Status.CREATED).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @PUT
    @Path("user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyUser(LoginUser user) {
        AdminUserDAO adao = new AdminUserDAOImp();
        try {
            if (adao.passwordCustomer(user.getEmail(), user.getPassword())) {
                return Response.status(Response.Status.ACCEPTED).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE // Eliminar un usuario
    @Path("user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUsuario(@PathParam("id") String id) {
        AdminUserDAO adao = new AdminUserDAOImp();
        try {
            adao.removeCustomer(id);
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE // Eliminar un usuario
    @Path("users")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteAllUsuario() {
        AdminUserDAO adao = new AdminUserDAOImp();
        ArrayList<Customer> customers;
        try {
            customers = adao.getCustomers();
            for (int i = 0; i < customers.size(); i++) {
                adao.removeCustomer(customers.get(i).getEmail());
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
