package es.usc.grei.aos.ejercicio1.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.vo.Valoration;

public class ValorationsDAOImp implements ValorationsDAO {

    @Override
    public ArrayList<Valoration> getValorations(String id) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM valorations WHERE id=" + id);
            ArrayList<Valoration> valorations = new ArrayList();
            while (rs.next()) {
                Valoration val = new Valoration(rs.getString("id"), rs.getString("customer"), rs.getInt("score"), rs.getString("comment"));
                valorations.add(val);
            }
            con.close();
            return valorations;
        }
    }

    @Override
    public void insertValoration(Valoration valoration) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            try {
                con.setTransactionIsolation(4);
                Statement st = con.createStatement();
                st.executeUpdate("INSERT INTO valorations (id,customer,score,comment) VALUES (" + valoration.getProductId() + ",'" + valoration.getUserEmail() + "'," + valoration.getScore() + ",'" + valoration.getComment() + "')");
                con.close();
            } catch (SQLException ex) {
                con.rollback();
                con.close();
                throw new SQLException();
            }
        }
    }
}
