package es.usc.grei.aos.ejercicio1.model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import es.usc.grei.aos.ejercicio1.model.Cart;
import model.vo.Bill;
import model.vo.Cd;
import model.vo.Product;

public interface ProductDAO {

    public Cd getCd(String id) throws SQLException;

    public String getType(String id) throws SQLException;

    public ArrayList<Product> getList(HashMap<String, String> filter) throws SQLException;

    public String buy(Cart cart, String shipAddress, String customerMail, Float multiplier) throws SQLException;

    public Product getProduct(String id) throws SQLException;

    public Bill getLastBill(String customer) throws SQLException;

    public boolean addProduct(Product product) throws SQLException;
}
