package es.usc.grei.aos.ejercicio1.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.vo.AdminUser;
import model.vo.Customer;

public class AdminUserDAOImp implements AdminUserDAO {

    @Override
    public AdminUser getAdmin(String email, String password) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            AdminUser adminUser;
            adminUser = null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM administrators WHERE email='" + email + "' AND password = '" + password + "'");
            if (rs.next()) {
                adminUser = new AdminUser(rs.getString("name"), email, password);
            }
            con.close();
            return adminUser;
        }
    }

    @Override
    public boolean passwordCustomer(String email, String password) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            try {
                con.setTransactionIsolation(4);
                Statement st = con.createStatement();
                if (email != null && password != null) {
                    st.executeUpdate("UPDATE customers SET password = '" + password + "' WHERE email = '" + email + "'");
                    con.close();
                    return true;
                }
                return false;
            } catch (SQLException ex) {
                con.rollback();
                con.close();
                throw new SQLException();
            }
        }
    }

    @Override
    public boolean removeCustomer(String email) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            try {
                con.setTransactionIsolation(4);
                Statement st = con.createStatement();
                if (email != null) {
                    st.executeUpdate("DELETE FROM customers WHERE email = '" + email + "'");
                    con.close();
                    return true;
                }
                return false;
            } catch (SQLException ex) {
                con.rollback();
                con.close();
                throw new SQLException();
            }
        }
    }

    @Override
    public ArrayList<Customer> getCustomers() throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            ArrayList<Customer> customers;
            customers = new ArrayList();
            Customer customer;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM customers");
            while (rs.next()) {
                customer = new Customer(rs.getFloat("expenditure"), rs.getString("address"), rs.getString("name"), rs.getString("email"), rs.getString("password"));
                customers.add(customer);
            }
            con.close();
            return customers;
        }
    }
}
