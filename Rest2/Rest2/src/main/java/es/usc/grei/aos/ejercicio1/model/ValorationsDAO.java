package es.usc.grei.aos.ejercicio1.model;

import java.sql.SQLException;
import java.util.ArrayList;
import model.vo.Valoration;

public interface ValorationsDAO {

    public ArrayList<Valoration> getValorations(String id) throws SQLException;

    public void insertValoration(Valoration valoration) throws SQLException;

}
