/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio1.model;

import java.sql.SQLException;
import java.util.ArrayList;
import model.vo.AdminUser;
import model.vo.Customer;

/**
 *
 * @author J.Guzmán
 */
public interface AdminUserDAO {

    public AdminUser getAdmin(String email, String password) throws SQLException;

    public boolean passwordCustomer(String email, String password) throws SQLException;

    public boolean removeCustomer(String email) throws SQLException;
    public ArrayList<Customer> getCustomers() throws SQLException;
}
