/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio1.model;

import javax.ws.rs.FormParam;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author jguzman
 */
public class LoginUser {

    @FormParam("email")
    private String email;
    @FormParam("password")
    private String password;

    public LoginUser(String id, String email) {
        this.email = id;
        this.password = email;
    }

    @Override
    public String toString() {
        try {
            // takes advantage of toString() implementation to format {"a":"b"}
            return new JSONObject().put("email", email).put("password", password).toString();
        } catch (JSONException e) {
            return null;
        }
    }

    public LoginUser() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String id) {
        this.email = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String email) {
        this.password = email;
    }
}
