/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio1.model;

import java.sql.SQLException;
import java.util.ArrayList;
import model.vo.Customer;

/**
 *
 * @author J.Guzmán
 */
public interface CustomerDAO {

    public Customer getCustomer(String email, String password) throws SQLException;

    public Customer getCustomer(String email) throws SQLException;

    public void insertCustomer(Customer customer) throws SQLException;

    public boolean hasPurchasedProduct(String mail, String id) throws SQLException;
}
