package es.usc.grei.aos.ejercicio1.ws;

import es.usc.grei.aos.ejercicio1.model.MySQLConnection;
import es.usc.grei.aos.ejercicio1.model.ProductDAO;
import es.usc.grei.aos.ejercicio1.model.ProductDAOImp;
import es.usc.grei.aos.ejercicio1.model.User;
import es.usc.grei.aos.ejercicio1.model.ValorationsDAO;
import es.usc.grei.aos.ejercicio1.model.ValorationsDAOImp;
import es.usc.grei.aos.ejercicio1.model.WSResult;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.vo.Cd;
import model.vo.Product;
import model.vo.ProductData;
import model.vo.Valoration;

/**
 * REST Web Service
 *
 * @author jguzman
 */
@Path("/")
public class ProductResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserResource
     */
    public ProductResource() {
    }

    @GET
    @Path("products")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllProducts() throws SQLException {
        ProductDAO pDAO = new ProductDAOImp();
        ArrayList<Product> products = pDAO.getList(null);
        if (products == null || products.size() == 0) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(new WSResult(true, products)).build();
        }
    }

    @GET
    @Path("product/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProduct(@PathParam("id") String id) {
        ArrayList<Valoration> valorations;
        Product p;
        ProductDAO pDAO = new ProductDAOImp();
        ValorationsDAO daoValorations = new ValorationsDAOImp();

        try {
            p = pDAO.getProduct(id);
            valorations = daoValorations.getValorations(id);
            if (p == null || valorations == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            if (p.getType().equals("cd")) {
                ProductData pdata = new ProductData((Cd) p, valorations);
                return Response.ok(new WSResult(true, pdata)).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

    }

    @GET
    @Path("product/search/{title}/{author}/{maxPrice}/{year}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchProduct(@PathParam("title") String title, @PathParam("author") String author,
            @PathParam("maxPrice") String maxPrice, @PathParam("year") String year) {

        ProductDAO pdao = new ProductDAOImp();
        HashMap<String, String> filter = new HashMap<>();
        if (!title.equals("undefined")) {
            filter.put("title", title);
        }
        if (!author.equals("undefined")) {
            filter.put("author", author);
        }
        if (!maxPrice.equals("undefined")) {
            filter.put("maxPrice", maxPrice);
        }
        if (!year.equals("undefined")) {
            filter.put("year", year);
        }
        try {
            ArrayList<Product> products = pdao.getList(filter);
            return Response.ok(new WSResult(true, products)).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
