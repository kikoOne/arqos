package model.dao;

public class DAOFactoryImp implements DAOFactory {

    @Override
    public CustomerDAO getCustomerDAO() {

        CustomerDAO customerDAO = new CustomerDAOImp();
        return customerDAO;

    }

//    @Override
//    public AdminUserDAO getAdminUserDAO() {
//
//        AdminUserDAO adminUserDAO = new AdminUserDAOImp();
//        return adminUserDAO;
//
//    }

    @Override
    public ProductDAO getProductDAO() {

        ProductDAO cdDao = new ProductDAOImp();
        return cdDao;

    }

//    @Override
//    public ValorationsDAO getValorationsDAO() {
//
//        ValorationsDAO cdDao = new ValorationsDAOImp();
//        return cdDao;
//
//    }
}
