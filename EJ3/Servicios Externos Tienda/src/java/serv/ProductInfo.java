package serv;

import java.sql.SQLException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.vo.Cd;
import model.vo.Product;

/**
 *
 * @author jguzman
 */
@XmlTransient
@XmlSeeAlso({Product.class, Cd.class})
@WebService(serviceName = "ProductInfo")
public class ProductInfo {

    private final ProductDAO pDAO;

    public ProductInfo() {
        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        this.pDAO = daoFactory.getProductDAO();
    }

    @WebMethod(operationName = "getProduct")
    public Product getProduct(@WebParam(name = "id") String id) throws ExceptionRemote {
        try {
            Product p = pDAO.getProduct(id);
            return p;
        } catch (SQLException ex) {
            ExceptionRemote er = new ExceptionRemote(new ExceptionRemoteBean("Error en acceso a la base de datos"));
            throw er;
        }
    }
}
