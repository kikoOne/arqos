package serv;

import java.sql.SQLException;
import java.util.ArrayList;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.jws.Oneway;
import javax.jws.WebService;
import javax.jws.WebMethod;
//import javax.jws.WebParam;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.vo.Cd;
import model.vo.Product;

/**
 *
 * @author jguzman
 */
@XmlTransient 
@XmlSeeAlso({Product.class, Cd.class})
@WebService(serviceName = "Catalogue")
public class Catalogue {

    private final ProductDAO pDAO;

    public Catalogue() {
        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        this.pDAO = daoFactory.getProductDAO();
    }

    @WebMethod(operationName = "getCatalogue")
    public ArrayList<Product> getCatalogue() throws ExceptionRemote {
        
        try {
            ArrayList<Product> list = pDAO.getList(null, null);
            return list;
        } catch (SQLException ex) {
            ExceptionRemote er = new ExceptionRemote(new ExceptionRemoteBean("Error en acceso a la base de datos"));
            throw er;
        }
    }
}
