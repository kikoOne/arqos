package model.service;

import java.util.List;
import services.Cd;

/**
 *
 * @author J.Guzmán
 */
public class ExternServices {

    public static List<Cd> callService(String user, String password, String currency, String type, boolean sendEmail) {
        javax.xml.ws.Holder<java.lang.String> result = new javax.xml.ws.Holder<>();
        javax.xml.ws.Holder<services.ConvertCurrencyResponse> products = new javax.xml.ws.Holder<>();
        shopWSDLOperation(user, password, currency, type, sendEmail, result, products);
        System.out.println("Result" + result.value);
        return products.value.getReturn();
    }

    public static void shopWSDLOperation(java.lang.String user, java.lang.String password, java.lang.String currency, java.lang.String type, boolean sendEmail, javax.xml.ws.Holder<java.lang.String> result, javax.xml.ws.Holder<services.ConvertCurrencyResponse> products) {
        services.ShopWSDLService service = new services.ShopWSDLService();
        services.ShopWSDLPortType port = service.getShopWSDLPort();
        port.shopWSDLOperation(user, password, currency, type, sendEmail, result, products);
    }

    public static Product getProduct(java.lang.String id) throws ExceptionRemote_Exception {
        model.service.ProductInfo_Service service = new model.service.ProductInfo_Service();
        model.service.ProductInfo port = service.getProductInfoPort();
        return port.getProduct(id);
    }
}
