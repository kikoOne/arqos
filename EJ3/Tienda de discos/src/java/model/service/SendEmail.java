package model.service;

import java.text.DecimalFormat;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.http.HttpServletRequest;
import model.vo.Customer;

public class SendEmail {

    private String to;
    private final String text;
    private final HttpServletRequest request;

    public SendEmail(HttpServletRequest request) {
        Customer customer = (Customer) request.getSession().getAttribute("customer");
        this.to = customer.getEmail();
        this.text = customer.getName();
        this.request = request;
    }

    public void send() {
        DecimalFormat format = new DecimalFormat("##########.##");
        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.user", "tiendadediscos@gradox.org");
        props.put("mail.smtp.password", "guzman88");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", true);

        Session session = Session.getInstance(props, null);
        MimeMessage message = new MimeMessage(session);

        // Create the email addresses involved
        try {
            InternetAddress from = new InternetAddress("tiendadediscos@gradox.org");
            message.setSubject("Compra completada");
            message.setFrom(from);
            message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

            // Create a multi-part to combine the parts
            Multipart multipart = new MimeMultipart("alternative");

            // Create your text message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Add the text part to the multipart
            multipart.addBodyPart(messageBodyPart);
            messageBodyPart.setText("");

            // Create the html part
            messageBodyPart = new MimeBodyPart();
            String htmlMessage = text + ", su compra se ha completado con éxito y está siendo preparada para el envío.<br><br>";
            htmlMessage += "<table cellpadding=\"10\"><tr><td>Identificador</td><td>Título</td><td>Cantidad</td><td>Precio total + IVA</td></tr>";
            Cart cart = (Cart) request.getSession().getAttribute("cart");
            Customer customer = (Customer) request.getSession().getAttribute("customer");
            Float multiplier = 1F;
            if (customer.getExpenditure() - cart.getTotalVat() >= 100) {
                multiplier = 0.8F;
            }
            for (String p : cart.getProducts().keySet()) {
                htmlMessage += "<tr><td>" + p + "</td><td>" + cart.getProducts().get(p).getProduct().getTitle() + "</td><td>" + cart.getProducts().get(p).getQuantity() + "</td><td>" + format.format(cart.getProducts().get(p).getProduct().getPrice() * multiplier * cart.getProducts().get(p).getQuantity() * cart.getProducts().get(p).getProduct().getVat()) + "$</td></tr>";
            }
            htmlMessage += "</table><br><br><i><b>TOTAL: " + format.format(cart.getTotalVat() * multiplier) + "$</b></i>";
            messageBodyPart.setContent(htmlMessage, "text/html");

            // Add html part to multi part
            multipart.addBodyPart(messageBodyPart);

            // Associate multi-part with message
            message.setContent(multipart);

            // Send message
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", "tiendadediscos@gradox.org", "guzman88");
            System.out.println("Transport: " + transport.toString());
            transport.sendMessage(message, message.getAllRecipients());
        } catch (AddressException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        } catch (MessagingException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }

    }
}
