package model.dao;

public class DAOFactoryCreator {

    public DAOFactory getDAOFactory() {
        DAOFactory daoFactory = new DAOFactoryImp();
        return daoFactory;
    }
}
