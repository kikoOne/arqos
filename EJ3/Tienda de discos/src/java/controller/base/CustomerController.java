package controller.base;

import controller.helper.*;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.service.Cart;
import model.service.ExternServices;
import model.service.SendEmail;
import model.vo.Customer;
import model.vo.User;

public class CustomerController extends HttpServlet {

    private CartHelper carth;
    private CatalogHelper catalogh;
    private LoginHelper loginh;
    private PurchaseHelper purchaseh;
    private RegisterHelper registerh;
    private SearchHelper searchh;
    private ValorationHelper valorationh;
    private BillHelper billh;

    public CustomerController() {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void gotoPage(String page, HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher(page).forward(request, response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(controller.base.CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void externServiceCall(HttpServletRequest request, HttpServletResponse response, String filter) {
        Customer c = (Customer) request.getSession().getAttribute("customer");
        String currency;
        if (request.getSession(false).getAttribute("customer") == null) {
            loginh = new LoginHelper(request);
            c = new Customer(loginh.getEmail(), loginh.getEmail(), loginh.getPassword());
        }
        if (request.getParameter("currency") != null) {
            currency = (String) request.getParameter("currency");
        } else {
            currency = (String) request.getSession().getAttribute("currency");
            if (currency == null) {
                currency = "USD";
            }
        }
        request.getSession().setAttribute("currency", currency);
        try {
            List<services.Cd> prod = ExternServices.callService(((User) c).getEmail(), ((User) c).getPassword(), currency, filter, false);
            request.getSession().setAttribute("products", prod);
            if (prod == null || prod.isEmpty()) {
                request.setAttribute("error", "Usuario o contraseña incorrectos.");
                gotoPage("customer/ErrorView.jsp", request, response);
            } else {
                request.getSession(true).setAttribute("customer", c);
                request.setAttribute("view", "Catálogo de discos");
                gotoPage("customer/CatalogView.jsp", request, response);

            }
        } catch (Exception ex) {
            request.setAttribute("error", ex.getMessage());
            gotoPage("customer/ErrorView.jsp", request, response);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        request.setAttribute("server", "true");
        String location = request.getParameter("location");
        if (location == null) {
            if (request.getSession().getAttribute("customer") == null) {
                gotoPage("customer/LoginView.jsp", request, response);
            } else {
                externServiceCall(request, response, "*");
            }
        } else {
            if (request.getSession().getAttribute("customer") != null || location.matches("login") || location.matches("gotoRegister") || location.matches("register") || location.matches("gotoLogin")) {
                Cart cart = (Cart) request.getSession(false).getAttribute("cart");
                switch (location) {
                    case "gotoLogin":
                        gotoPage("customer/LoginView.jsp", request, response);
                        break;
                    case "login":
                        externServiceCall(request, response, "*");
                        break;
                    case "logout":
                        loginh = new LoginHelper(request);
                        if (loginh.logout()) {
                            catalogh = new CatalogHelper(request);
                            catalogh.getCatalog();
                            request.setAttribute("view", "Catálogo de discos");
                            gotoPage("customer/LoginView.jsp", request, response);
                        } else {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        }
                        break;
                    case "addCart":
                        carth = new CartHelper(request);
                        if (carth.addCart()) {
                            gotoPage("customer/CartView.jsp", request, response);
                        } else {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        }
                        break;
                    case "removeCart":
                        carth = new CartHelper(request);
                        if (carth.removeCart()) {
                            gotoPage("customer/CartView.jsp", request, response);
                        } else {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        }
                        break;
                    case "showCart":
                        carth = new CartHelper(request);
                        if (!carth.logged()) {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        } else {
                            gotoPage("customer/CartView.jsp", request, response);
                        }
                        break;
                    case "startPurchase":
                        if (cart != null) {
                            if (cart.getProducts().size() > 0) {
                                gotoPage("customer/PurchaseConfirmationView.jsp", request, response);
                            } else {
                                gotoPage("customer/ErrorView.jsp", request, response);
                            }
                        } else {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        }
                        break;
                    case "confirmPurchase":
                        if (cart != null && request.getParameter("shipAddress") != null) {
                            request.getSession().setAttribute("shipAddress", request.getParameter("shipAddress"));
                            if (cart.getProducts().size() > 0) {
                                gotoPage("customer/FinishPurchaseView.jsp", request, response);
                            } else {
                                gotoPage("customer/ErrorView.jsp", request, response);
                            }
                        } else {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        }
                        break;
                    case "finishPurchase":
                        billh = new BillHelper(request);
                        purchaseh = new PurchaseHelper(request);
                        if (purchaseh.buy()) {
                            SendEmail se = new SendEmail(request);
                            se.send();
                            carth = new CartHelper(request);
                            carth.resetCart();
                            if (billh.getBill()) {
                                gotoPage("customer/BillView.jsp", request, response);
                            } else {
                                gotoPage("customer/ErrorView.jsp", request, response);
                            }
                        } else {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        }
                        break;
                    case "gotoRegister":
                        gotoPage("customer/RegisterView.jsp", request, response);
                        break;
                    case "register":
                        registerh = new RegisterHelper(request);
                        if (registerh.addCustomer()) {
                            gotoPage("customer/LoginView.jsp", request, response);
                        } else {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        }
                        break;
                    case "gotoSearch":
                        gotoPage("customer/SearchView.jsp", request, response);
                        break;
                    case "searchProducts":
                        String author = request.getParameter("author");
                        if (author == null) {
                            author = (String) request.getSession().getAttribute("author");
                        }
                        externServiceCall(request, response, author);
                        request.getSession().setAttribute("author", author);
                        break;
                    case "seeProduct":
                        valorationh = new ValorationHelper(request);
                        String result = valorationh.getProductData();
                        if (result != null) {
                            if (result.equals("cd")) {
                                gotoPage("customer/CdView.jsp", request, response);
                            }
                        } else {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        }
                        break;
                    case "addValoration":
                        valorationh = new ValorationHelper(request);
                        if (valorationh.introduceValoration()) {
                            gotoPage("customer/SuccessView.jsp", request, response);
                        } else {
                            gotoPage("customer/ErrorView.jsp", request, response);
                        }
                        break;
                    default:
                        externServiceCall(request, response, "*");
                        break;
                }
            } else {
                gotoPage("customer/LoginView.jsp", request, response);
            }
        }
    }
}
