package controller.helper;

import javax.servlet.http.HttpServletRequest;
import model.service.ExternServices;
import model.vo.Customer;


public class LoginHelper {

    private final HttpServletRequest request;
    private final String email;
    private final String password;
    private Customer customer;

    public LoginHelper(HttpServletRequest request) {
        this.email = request.getParameter("email");
        this.password = request.getParameter("password");
        this.request = request;
    }

    public boolean logout() {
        if (request.getSession(false) != null) { // Si hay una sesión
            if (request.getSession(false).getAttribute("customer") != null) {
                request.getSession().invalidate();
                return true;
            }
        }
        request.setAttribute("error", "No hay una sesión abierta.");
        return false;
    }

    public boolean login() {
//        // Si ya hay una sesión vuelve al catálogo
//        if (request.getSession(false) != null) { // Si hay una sesión
//            if (request.getSession(false).getAttribute("customer") != null) {
//                return true;
//            }
//        }
//        
//        
//        try {
//            customer = ExternServices.login(email, password);
//            if (customer != null) {
//                request.getSession(true).setAttribute("customer", customer);
//                return true;
//            } else {
//                request.setAttribute("error", "Usuario o contraseña incorrectos.");
//                return false;
//            }
//        } catch (ExceptionRemote_Exception ex) {
//            request.setAttribute("error", ex.getMessage());
//            return false;
//        }
        return true;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
