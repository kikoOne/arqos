package controller.helper;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import model.dao.AdminUserDAO;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.vo.Customer;

public class ManageUserHelper {

    private final HttpServletRequest request;
    private final AdminUserDAO adminUserDao;

    public ManageUserHelper(HttpServletRequest request) {
        this.request = request;
        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        adminUserDao = daoFactory.getAdminUserDAO();
    }

    public boolean removeUser() {
        try {
            if (adminUserDao.removeCustomer(request.getParameter("email"))) {
                request.setAttribute("message", "Usuario eliminado correctamente.");
                return true;
            } else {
                request.setAttribute("error", "Error al eliminar el usuario de la base de datos.");
                return false;
            }
        } catch (SQLException ex) {
            request.setAttribute("error", "Error al eliminar el usuario de la base de datos.");
            return false;
        }
    }

    public boolean changePassword() {
        if (request.getSession().getAttribute("admin") != null) {
            try {
                if (adminUserDao.passwordCustomer(request.getParameter("email"), request.getParameter("password"))) {
                    request.setAttribute("message", "Contraseña cambiada correctamente.");
                    return true;
                } else {
                    request.setAttribute("error", "Error al cambiar la contraseña.");
                    return false;
                }
            } catch (SQLException ex) {
                request.setAttribute("error", "Error al cambiar la contraseña.");
                return false;
            }
        }
        request.setAttribute("error", "Debe iniciar sesión como administrador para cambiar la contraseña.");
        return false;
    }

    public boolean getUsers() {
        if (request.getSession().getAttribute("admin") != null) {
            try {
                ArrayList<Customer> customers = adminUserDao.getCustomers();
                request.setAttribute("customers", customers);
            } catch (SQLException ex) {
                request.setAttribute("error", "Error al recuperar los usuarios de la base de datos.");
                return false;
            }
            return true;
        }
        request.setAttribute("error", "Debe iniciar sesion para recuperar datos de todos los usuarios.");
        return false;
    }
}
