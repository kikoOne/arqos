package controller.helper;

import java.util.ArrayList;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;

public class SearchHelper {

//    private ArrayList<Product> list;
    private final HttpServletRequest request;

    public SearchHelper(HttpServletRequest request) {
        this.request = request;
    }

    public boolean getFilteredList() {

        Enumeration<String> parameters = request.getParameterNames();
        ArrayList<String> param = new ArrayList<>();
        ArrayList<String> data = new ArrayList<>();
        while (parameters.hasMoreElements()) {
            String temp = parameters.nextElement();
            param.add(temp);
            data.add(request.getParameter(temp));
        }

//        try {
//            list = (ArrayList<Product>) ExternServices.doFilter(param, data);
//        } catch (ExceptionRemote_Exception ex) {
//            request.setAttribute("error", ex.getMessage());
//            return false;
//        }
        
        //request.setAttribute("products", list);
        return true;
    }
}
