$(document).ready(function(){
	$win = $(window);
	$win.scroll(function(){
		$pos = 25;
		if ($win.scrollTop() > $pos){
			$("body > header").stop().animate({opacity: 0.9}, 200).addClass("headerScroll");
			$("body").addClass("bodyScroll");
		} else {
			$("body > header").stop().animate({opacity: 1}, 200).removeClass("headerScroll");
			$("body").removeClass("bodyScroll");
		}
	});
});