<%@include file="../frame/top.jsp" %>
<title>Cat�logo</title>
</head>
<body>
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Cat�logo de discos</h1>
        </header>
        <c:if test="${empty products}">
            No hay coincidencias o art�culos disponibles.
        </c:if>
        <table>
            <c:forEach var="product" items="${products}">
                <c:set var="boton" scope="page" value="A�adir al carrito"/>
                <c:set var="disabled" scope="page" value=""/>
                <c:if test="${product.stock == 0}">
                    <c:set var="disabled" scope="page" value="disabled"/>
                    <c:set var="boton" scope="page" value="No disponible"/>
                </c:if>

                <tr>
                    <td>
                        <a href="admin?location=seeProduct&id=${product.id}">
                            <object data="./images/products/${product.id}.jpg" type="image/png" height="100">
                                <img src="./images/products/none.jpg" height="100">
                            </object>
                        </a>
                        <br>
                    </td>                
                    <td><a href="admin?location=seeProduct&id=${product.id}">${product.title}</a></td>
                    <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${product.price * 1.21}"/>$</td>
                    <td>${product.year}</td>
                    <td>${product.stock} uds.</td>
                </tr>
            </c:forEach>
        </table> 
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>