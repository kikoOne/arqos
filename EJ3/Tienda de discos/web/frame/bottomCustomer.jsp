<footer>
    Tienda de discos &copy; 2015 - <a href="admin">Zona de administración</a>
    <br>
    <br>
    <c:choose>
        <c:when test="${empty customer}"> 
            <a href="store?location=gotoLogin">Iniciar sesión</a>
        </c:when>
        <c:otherwise>   
            ${customer.email} | <a href="store?location=logout">Cerrar sesión</a>
        </c:otherwise>
    </c:choose>
</footer>
</html>