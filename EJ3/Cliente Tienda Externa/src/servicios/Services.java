package servicios;

import serv.Customer;
import serv.ExceptionRemote_Exception;
import serv3.Product;

/**
 *
 * @author jguzman
 */
public class Services {

    public static Customer login(java.lang.String email, java.lang.String password) throws ExceptionRemote_Exception {
        serv.LoginService_Service service = new serv.LoginService_Service();
        serv.LoginService port = service.getLoginServicePort();
        return port.login(email, password);
    }

    public static java.util.List<serv2.Product> getCatalogue() throws serv2.ExceptionRemote_Exception {
        serv2.Catalogue_Service service = new serv2.Catalogue_Service();
        serv2.Catalogue port = service.getCataloguePort();
        return port.getCatalogue();
    }

    public static Product getProduct(java.lang.String id) throws serv3.ExceptionRemote_Exception {
        serv3.ProductInfo_Service service = new serv3.ProductInfo_Service();
        serv3.ProductInfo port = service.getProductInfoPort();
        return port.getProduct(id);
    }

    public static java.util.List<serv4.Product> doFilter(java.util.List<java.lang.String> input, java.util.List<java.lang.String> data) throws serv4.ExceptionRemote_Exception {
        serv4.Filter_Service service = new serv4.Filter_Service();
        serv4.Filter port = service.getFilterPort();
        return port.doFilter(input, data);
    }
}
