package gui;

import java.util.ArrayList;
import java.util.List;
import javax.swing.text.DefaultCaret;
import serv2.Cd;
import serv4.ExceptionRemote_Exception;
import serv4.Product;
import servicios.Services;

/**
 *
 * @author jguzman
 */
public class GUI_filter extends javax.swing.JFrame {

    public GUI_filter() {
        initComponents();
        DefaultCaret caret = (DefaultCaret) area.getCaret();
        caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        author = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        canvas1 = new java.awt.Canvas();
        jLabel2 = new javax.swing.JLabel();
        year = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        title = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        max = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        area = new javax.swing.JTextArea();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        idText = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Filtrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getProducts(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Catálogo");

        jLabel2.setText("Autor:");

        jLabel3.setText("Año:");

        jLabel4.setText("Título:");

        jLabel5.setText("Precio máx:");

        area.setBackground(new java.awt.Color(240, 240, 240));
        area.setColumns(20);
        area.setRows(5);
        jScrollPane1.setViewportView(area);

        jButton2.setText("Ver catálogo");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getCatalogue(evt);
            }
        });

        jButton3.setText("Ver producto");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewProduct(evt);
            }
        });

        jLabel6.setText("Id:");

        jButton4.setText("Log out");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logout(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE)
                        .addGap(99, 99, 99))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(2, 2, 2)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 824, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGap(28, 28, 28)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel4))
                            .addGap(23, 23, 23)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(author, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(title, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(37, 37, 37)
                                    .addComponent(jLabel5)))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(max, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(year, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(42, 42, 42)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jButton2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, Short.MAX_VALUE)))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(idText)
                                .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(canvas1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jButton4))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(max, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jButton2)
                    .addComponent(idText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(year, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(author, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(canvas1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(192, 192, 192))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void getProducts(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getProducts

        boolean flag = false;
        List<String> whiteList = new ArrayList<>();
        List<String> data = new ArrayList<>();
        area.setText("");

        if (!title.getText().equals("")) {
            whiteList.add("title");
            data.add(title.getText());
            flag = true;
        }
        if (!author.getText().equals("")) {
            whiteList.add("author");
            data.add(author.getText());
            flag = true;
        }
        if (!max.getText().equals("")) {
            whiteList.add("maxPrice");
            data.add(max.getText());
            flag = true;
        }
        if (!year.getText().equals("")) {
            whiteList.add("year");
            data.add(year.getText());
            flag = true;
        }
        if (!flag) {
            try {
                List<serv2.Product> list = Services.getCatalogue();
                for (serv2.Product l : list) {
                    area.append(l.getTitle() + "\n" + ((Cd) l).getAuthor() + "\n" + ((Cd) l).getPrice() + "\n" + ((Cd) l).getStock() + "\n");
                    area.append("\n-------------------------------------------------------------\n");
                }
            } catch (serv2.ExceptionRemote_Exception ex) {
                Popup popup = new Popup(ex.getMessage());
                popup.setVisible(true);
            }
        } else {
            try {
                ArrayList<Product> list = (ArrayList<Product>) Services.doFilter(whiteList, data);
                for (Product l : list) {
                    area.append(l.getTitle() + "\n" + ((serv4.Cd) l).getAuthor() + "\n" + ((serv4.Cd) l).getPrice() + "\n" + ((serv4.Cd) l).getStock());
                    area.append("\n-------------------------------------------------------------\n");
                }
            } catch (ExceptionRemote_Exception ex) {
                Popup popup = new Popup(ex.getMessage());
                popup.setVisible(true);
            }
        }
    }//GEN-LAST:event_getProducts

    private void getCatalogue(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getCatalogue
        area.setText("");
        try {
            List<serv2.Product> list = Services.getCatalogue();
            for (serv2.Product l : list) {
                area.append(l.getTitle() + "\n" + ((Cd) l).getAuthor() + "\n" + ((Cd) l).getBarCode() + "\n" + ((Cd) l).getCountry()
                        + "\n" + ((Cd) l).getPrice() + "\n" + ((Cd) l).getStock() + "\n" + ((Cd) l).getDescription());
                area.append("\n-------------------------------------------------------------\n");
            }
        } catch (serv2.ExceptionRemote_Exception ex) {
            Popup popup = new Popup(ex.getMessage());
            popup.setVisible(true);
        }
    }//GEN-LAST:event_getCatalogue

    private void logout(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logout
        this.dispose();
        GUI_login log = new GUI_login();
        log.setLocationRelativeTo(null);
        log.setVisible(true);
    }//GEN-LAST:event_logout

    private void viewProduct(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewProduct
        String id = idText.getText();
        area.setText("");
        try {
            serv3.Product p = Services.getProduct(id);
            area.append(p.getTitle() + "\n" + ((serv3.Cd) p).getAuthor() + "\n" + ((serv3.Cd) p).getBarCode() + "\n" + ((serv3.Cd) p).getCountry()
                    + "\n" + ((serv3.Cd) p).getPrice() + "\n" + ((serv3.Cd) p).getStock() + "\n" + ((serv3.Cd) p).getDescription());
        } catch (serv3.ExceptionRemote_Exception ex) {
            Popup popup = new Popup(ex.getMessage());
            popup.setVisible(true);
        }
    }//GEN-LAST:event_viewProduct

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea area;
    private javax.swing.JTextField author;
    private java.awt.Canvas canvas1;
    private javax.swing.JTextField idText;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField max;
    private javax.swing.JTextField title;
    private javax.swing.JTextField year;
    // End of variables declaration//GEN-END:variables
}
