
package analisistexto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the analisistexto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FrecuenciaPalabrasResponse_QNAME = new QName("http://AnalisisTexto/", "frecuencia_palabrasResponse");
    private final static QName _NumeroCaracteres_QNAME = new QName("http://AnalisisTexto/", "numero_caracteres");
    private final static QName _ContarPalabras_QNAME = new QName("http://AnalisisTexto/", "contar_palabras");
    private final static QName _FrecuenciaPalabras_QNAME = new QName("http://AnalisisTexto/", "frecuencia_palabras");
    private final static QName _ContarPalabrasResponse_QNAME = new QName("http://AnalisisTexto/", "contar_palabrasResponse");
    private final static QName _NumeroCaracteresResponse_QNAME = new QName("http://AnalisisTexto/", "numero_caracteresResponse");
    private final static QName _MayorFrecuenciaResponse_QNAME = new QName("http://AnalisisTexto/", "mayor_frecuenciaResponse");
    private final static QName _MayorFrecuencia_QNAME = new QName("http://AnalisisTexto/", "mayor_frecuencia");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: analisistexto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FrecuenciaPalabrasResponse }
     * 
     */
    public FrecuenciaPalabrasResponse createFrecuenciaPalabrasResponse() {
        return new FrecuenciaPalabrasResponse();
    }

    /**
     * Create an instance of {@link FrecuenciaPalabras }
     * 
     */
    public FrecuenciaPalabras createFrecuenciaPalabras() {
        return new FrecuenciaPalabras();
    }

    /**
     * Create an instance of {@link ContarPalabras }
     * 
     */
    public ContarPalabras createContarPalabras() {
        return new ContarPalabras();
    }

    /**
     * Create an instance of {@link NumeroCaracteres }
     * 
     */
    public NumeroCaracteres createNumeroCaracteres() {
        return new NumeroCaracteres();
    }

    /**
     * Create an instance of {@link MayorFrecuenciaResponse }
     * 
     */
    public MayorFrecuenciaResponse createMayorFrecuenciaResponse() {
        return new MayorFrecuenciaResponse();
    }

    /**
     * Create an instance of {@link ContarPalabrasResponse }
     * 
     */
    public ContarPalabrasResponse createContarPalabrasResponse() {
        return new ContarPalabrasResponse();
    }

    /**
     * Create an instance of {@link NumeroCaracteresResponse }
     * 
     */
    public NumeroCaracteresResponse createNumeroCaracteresResponse() {
        return new NumeroCaracteresResponse();
    }

    /**
     * Create an instance of {@link MayorFrecuencia }
     * 
     */
    public MayorFrecuencia createMayorFrecuencia() {
        return new MayorFrecuencia();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FrecuenciaPalabrasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://AnalisisTexto/", name = "frecuencia_palabrasResponse")
    public JAXBElement<FrecuenciaPalabrasResponse> createFrecuenciaPalabrasResponse(FrecuenciaPalabrasResponse value) {
        return new JAXBElement<FrecuenciaPalabrasResponse>(_FrecuenciaPalabrasResponse_QNAME, FrecuenciaPalabrasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumeroCaracteres }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://AnalisisTexto/", name = "numero_caracteres")
    public JAXBElement<NumeroCaracteres> createNumeroCaracteres(NumeroCaracteres value) {
        return new JAXBElement<NumeroCaracteres>(_NumeroCaracteres_QNAME, NumeroCaracteres.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContarPalabras }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://AnalisisTexto/", name = "contar_palabras")
    public JAXBElement<ContarPalabras> createContarPalabras(ContarPalabras value) {
        return new JAXBElement<ContarPalabras>(_ContarPalabras_QNAME, ContarPalabras.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FrecuenciaPalabras }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://AnalisisTexto/", name = "frecuencia_palabras")
    public JAXBElement<FrecuenciaPalabras> createFrecuenciaPalabras(FrecuenciaPalabras value) {
        return new JAXBElement<FrecuenciaPalabras>(_FrecuenciaPalabras_QNAME, FrecuenciaPalabras.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContarPalabrasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://AnalisisTexto/", name = "contar_palabrasResponse")
    public JAXBElement<ContarPalabrasResponse> createContarPalabrasResponse(ContarPalabrasResponse value) {
        return new JAXBElement<ContarPalabrasResponse>(_ContarPalabrasResponse_QNAME, ContarPalabrasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumeroCaracteresResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://AnalisisTexto/", name = "numero_caracteresResponse")
    public JAXBElement<NumeroCaracteresResponse> createNumeroCaracteresResponse(NumeroCaracteresResponse value) {
        return new JAXBElement<NumeroCaracteresResponse>(_NumeroCaracteresResponse_QNAME, NumeroCaracteresResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MayorFrecuenciaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://AnalisisTexto/", name = "mayor_frecuenciaResponse")
    public JAXBElement<MayorFrecuenciaResponse> createMayorFrecuenciaResponse(MayorFrecuenciaResponse value) {
        return new JAXBElement<MayorFrecuenciaResponse>(_MayorFrecuenciaResponse_QNAME, MayorFrecuenciaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MayorFrecuencia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://AnalisisTexto/", name = "mayor_frecuencia")
    public JAXBElement<MayorFrecuencia> createMayorFrecuencia(MayorFrecuencia value) {
        return new JAXBElement<MayorFrecuencia>(_MayorFrecuencia_QNAME, MayorFrecuencia.class, null, value);
    }

}
