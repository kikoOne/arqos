
package calculadoraavanzada;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the calculadoraavanzada package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Maximo_QNAME = new QName("http://CalculadoraAvanzada/", "maximo");
    private final static QName _Media_QNAME = new QName("http://CalculadoraAvanzada/", "media");
    private final static QName _MinimoResponse_QNAME = new QName("http://CalculadoraAvanzada/", "minimoResponse");
    private final static QName _MaximoResponse_QNAME = new QName("http://CalculadoraAvanzada/", "maximoResponse");
    private final static QName _MediaResponse_QNAME = new QName("http://CalculadoraAvanzada/", "mediaResponse");
    private final static QName _Minimo_QNAME = new QName("http://CalculadoraAvanzada/", "minimo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: calculadoraavanzada
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MaximoResponse }
     * 
     */
    public MaximoResponse createMaximoResponse() {
        return new MaximoResponse();
    }

    /**
     * Create an instance of {@link MediaResponse }
     * 
     */
    public MediaResponse createMediaResponse() {
        return new MediaResponse();
    }

    /**
     * Create an instance of {@link Minimo }
     * 
     */
    public Minimo createMinimo() {
        return new Minimo();
    }

    /**
     * Create an instance of {@link Maximo }
     * 
     */
    public Maximo createMaximo() {
        return new Maximo();
    }

    /**
     * Create an instance of {@link Media }
     * 
     */
    public Media createMedia() {
        return new Media();
    }

    /**
     * Create an instance of {@link MinimoResponse }
     * 
     */
    public MinimoResponse createMinimoResponse() {
        return new MinimoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Maximo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://CalculadoraAvanzada/", name = "maximo")
    public JAXBElement<Maximo> createMaximo(Maximo value) {
        return new JAXBElement<Maximo>(_Maximo_QNAME, Maximo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Media }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://CalculadoraAvanzada/", name = "media")
    public JAXBElement<Media> createMedia(Media value) {
        return new JAXBElement<Media>(_Media_QNAME, Media.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MinimoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://CalculadoraAvanzada/", name = "minimoResponse")
    public JAXBElement<MinimoResponse> createMinimoResponse(MinimoResponse value) {
        return new JAXBElement<MinimoResponse>(_MinimoResponse_QNAME, MinimoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaximoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://CalculadoraAvanzada/", name = "maximoResponse")
    public JAXBElement<MaximoResponse> createMaximoResponse(MaximoResponse value) {
        return new JAXBElement<MaximoResponse>(_MaximoResponse_QNAME, MaximoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MediaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://CalculadoraAvanzada/", name = "mediaResponse")
    public JAXBElement<MediaResponse> createMediaResponse(MediaResponse value) {
        return new JAXBElement<MediaResponse>(_MediaResponse_QNAME, MediaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Minimo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://CalculadoraAvanzada/", name = "minimo")
    public JAXBElement<Minimo> createMinimo(Minimo value) {
        return new JAXBElement<Minimo>(_Minimo_QNAME, Minimo.class, null, value);
    }

}
