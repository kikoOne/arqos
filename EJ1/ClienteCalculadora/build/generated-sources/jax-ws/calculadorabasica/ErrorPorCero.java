
package calculadorabasica;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ErrorPorCero complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ErrorPorCero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errorBean" type="{http://calculadoraBasica/}errorPorCeroBean" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorPorCero", propOrder = {
    "errorBean",
    "message"
})
public class ErrorPorCero {

    protected ErrorPorCeroBean errorBean;
    protected String message;

    /**
     * Obtiene el valor de la propiedad errorBean.
     * 
     * @return
     *     possible object is
     *     {@link ErrorPorCeroBean }
     *     
     */
    public ErrorPorCeroBean getErrorBean() {
        return errorBean;
    }

    /**
     * Define el valor de la propiedad errorBean.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorPorCeroBean }
     *     
     */
    public void setErrorBean(ErrorPorCeroBean value) {
        this.errorBean = value;
    }

    /**
     * Obtiene el valor de la propiedad message.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Define el valor de la propiedad message.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

}
