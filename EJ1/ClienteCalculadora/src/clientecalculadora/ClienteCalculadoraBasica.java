/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clientecalculadora;

import calculadorabasica.ErrorPorCero_Exception;

/**
 *
 * @author J.Guzmán
 */
public class ClienteCalculadoraBasica {

    public static float suma(float x, float y) {
        calculadorabasica.CalculadoraBasica_Service service = new calculadorabasica.CalculadoraBasica_Service();
        calculadorabasica.CalculadoraBasica port = service.getCalculadoraBasicaPort();
        return port.suma(x, y);
    }

    public static float resta(float x, float y) {
        calculadorabasica.CalculadoraBasica_Service service = new calculadorabasica.CalculadoraBasica_Service();
        calculadorabasica.CalculadoraBasica port = service.getCalculadoraBasicaPort();
        return port.resta(x, y);
    }

    public static float multiplicacion(float x, float y) {
        calculadorabasica.CalculadoraBasica_Service service = new calculadorabasica.CalculadoraBasica_Service();
        calculadorabasica.CalculadoraBasica port = service.getCalculadoraBasicaPort();
        return port.multiplicacion(x, y);
    }

    public static float division(float x, float y) throws ErrorPorCero_Exception {
        calculadorabasica.CalculadoraBasica_Service service = new calculadorabasica.CalculadoraBasica_Service();
        calculadorabasica.CalculadoraBasica port = service.getCalculadoraBasicaPort();
        return port.division(x, y);
    }
    
}
