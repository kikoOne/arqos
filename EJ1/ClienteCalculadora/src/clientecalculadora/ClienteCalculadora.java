package clientecalculadora;

import calculadorabasica.ErrorPorCero_Exception;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author J.Guzmán
 */
public class ClienteCalculadora {

    /**
     * @param args the command line arguments
     */
//    public static void main(String[] args) {
//        menu();
//    }
    
    public static void menu() {
        String op = "", txt = "", find = "";
        Float x, y;
        ArrayList<Float> arr;
        Scanner sn = new Scanner(System.in);
        do {
            System.out.println("-----------------MENU-----------------");
            System.out.println("a. Calculadora Basica");
            System.out.println("b. Calculadora Avanzada");
            System.out.println("c. Analizador de texto");
            System.out.println("x. Salir");
            System.out.println("Introducte tu elección: ");
            
            op = sn.nextLine();
            switch (op) {
                case "a":
                    System.out.println("-----------------CALCULADORA BASICA-----------------");
                    System.out.println("a. Suma");
                    System.out.println("b. Resta");
                    System.out.println("c. Multiplicacion");
                    System.out.println("d. Division");
                    System.out.println("x. Salir");
                    System.out.println("Introducte tu elección: ");
                    
                    op = sn.nextLine();
                    switch (op) {
                        case "a":
                            System.out.println("Primer operando");
                            x = sn.nextFloat();
                            System.out.println("Segundo operando");
                            y = sn.nextFloat();
                            System.out.println("Resultado");
                            System.out.println(ClienteCalculadoraBasica.suma(x, y));
                            break;
                        case "b":
                            System.out.println("Primer operando");
                            x = sn.nextFloat();
                            System.out.println("Segundo operando");
                            y = sn.nextFloat();
                            System.out.println("Resultado");
                            System.out.println(ClienteCalculadoraBasica.resta(x, y));
                            break;
                        case "c":
                            System.out.println("Primer operando");
                            x = sn.nextFloat();
                            System.out.println("Segundo operando");
                            y = sn.nextFloat();
                            System.out.println("Resultado");
                            System.out.println(ClienteCalculadoraBasica.multiplicacion(x, y));
                            break;
                        case "d":
                            System.out.println("Primer operando");
                            x = sn.nextFloat();
                            System.out.println("Segundo operando");
                            y = sn.nextFloat();
                            try {
                                System.out.println("Resultado");
                                System.out.println(ClienteCalculadoraBasica.division(x, y));
                            } catch (ErrorPorCero_Exception ex) {
                                System.out.println(ex.getFaultInfo().getMessage());
                            }
                            break;
                        case "x":
                            System.out.println("Volviendo a menu principal");
                            break;
                    }
                    break;
                case "b":
                    System.out.println("-----------------CALCULADORA AVANZADA-----------------");
                    System.out.println("a. Maximo");
                    System.out.println("b. Minimo");
                    System.out.println("c. Media");
                    System.out.println("x. Salir");
                    System.out.println("Introducte tu elección: ");
                    
                    op = sn.nextLine();
                    switch (op) {
                        case "a":
                            System.out.println("Cuantos numeros desea introducir");
                            x = sn.nextFloat();
                            arr = new ArrayList<>();
                            for (int i = 0; i < x; i++) {
                                System.out.println("Introduce un entero");
                                arr.add(sn.nextFloat());
                            }
                            System.out.println("Resultado");
                            System.out.println(ClienteCalculadoraAvanzada.maximo(arr));
                            break;
                        case "b":
                            System.out.println("Cuantos numeros desea introducir");
                            x = sn.nextFloat();
                            arr = new ArrayList<>();
                            for (int i = 0; i < x; i++) {
                                System.out.println("Introduce un entero");
                                arr.add(sn.nextFloat());
                            }
                            System.out.println("Resultado");
                            System.out.println(ClienteCalculadoraAvanzada.minimo(arr));
                            break;
                        case "c":
                            System.out.println("Cuantos numeros desea introducir");
                            x = sn.nextFloat();
                            arr = new ArrayList<>();
                            for (int i = 0; i < x; i++) {
                                System.out.println("Introduce un entero");
                                arr.add(sn.nextFloat());
                            }
                            System.out.println("Resultado");
                            System.out.println(ClienteCalculadoraAvanzada.media(arr));
                            break;
                        case "x":
                            System.out.println("Volviendo a menu principal");
                            break;
                    }
                    break;
                case "c":
                    System.out.println("-----------------ANALIZADOR DE TEXTO-----------------");
                    System.out.println("a. Contar palabras");
                    System.out.println("b. Contar caracteres");
                    System.out.println("c. Palabra mas repetida");
                    System.out.println("d. Frecuencia de una palabra");
                    System.out.println("x. Salir");
                    System.out.println("Introducte tu elección: ");
                    
                    op = sn.nextLine();
                    switch (op) {
                        case "a":
                            System.out.println("Introducir un texto");
                            txt = sn.nextLine();
                            System.out.println("Resultado");
                            System.out.println(ClienteAnalizadorTexto.contarPalabras(txt));
                            break;
                        case "b":
                            System.out.println("Introducir un texto");
                            txt = sn.nextLine();
                            System.out.println("Resultado");
                            System.out.println(ClienteAnalizadorTexto.numeroCaracteres(txt));
                            break;
                        case "c":
                            System.out.println("Introducir un texto");
                            txt = sn.nextLine();
                            System.out.println("Resultado");
                            System.out.println(ClienteAnalizadorTexto.mayorFrecuencia(txt));
                            break;
                        case "d":
                            System.out.println("Introducir un texto");
                            txt = sn.nextLine();
                            System.out.println("Introducir palabra que desee contar");
                            find = sn.nextLine();
                            System.out.println("Resultado");
                            System.out.println(ClienteAnalizadorTexto.frecuenciaPalabras(txt, find));
                            break;
                        case "x":
                            System.out.println("Volviendo a menu principal");
                            break;
                    }
                    break;
                case "x":
                    System.out.println("Saliendo....");
                    break;
            }
            sn.nextLine();
        } while (!op.equals("x"));
        
    }
}
