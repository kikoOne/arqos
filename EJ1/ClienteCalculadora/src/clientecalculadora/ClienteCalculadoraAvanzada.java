/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clientecalculadora;

/**
 *
 * @author J.Guzmán
 */
public class ClienteCalculadoraAvanzada {

    public static float maximo(java.util.List<java.lang.Float> x) {
        calculadoraavanzada.CalculadoraAvanzada_Service service = new calculadoraavanzada.CalculadoraAvanzada_Service();
        calculadoraavanzada.CalculadoraAvanzada port = service.getCalculadoraAvanzadaPort();
        return port.maximo(x);
    }

    public static float media(java.util.List<java.lang.Float> x) {
        calculadoraavanzada.CalculadoraAvanzada_Service service = new calculadoraavanzada.CalculadoraAvanzada_Service();
        calculadoraavanzada.CalculadoraAvanzada port = service.getCalculadoraAvanzadaPort();
        return port.media(x);
    }

    public static float minimo(java.util.List<java.lang.Float> x) {
        calculadoraavanzada.CalculadoraAvanzada_Service service = new calculadoraavanzada.CalculadoraAvanzada_Service();
        calculadoraavanzada.CalculadoraAvanzada port = service.getCalculadoraAvanzadaPort();
        return port.minimo(x);
    }
    
}
