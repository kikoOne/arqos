/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clientecalculadora;

/**
 *
 * @author J.Guzmán
 */
public class ClienteAnalizadorTexto {

    public static int contarPalabras(java.lang.String x) {
        analisistexto.AnalisisTexto_Service service = new analisistexto.AnalisisTexto_Service();
        analisistexto.AnalisisTexto port = service.getAnalisisTextoPort();
        return port.contarPalabras(x);
    }

    public static int frecuenciaPalabras(java.lang.String x, java.lang.String y) {
        analisistexto.AnalisisTexto_Service service = new analisistexto.AnalisisTexto_Service();
        analisistexto.AnalisisTexto port = service.getAnalisisTextoPort();
        return port.frecuenciaPalabras(x, y);
    }

    public static String mayorFrecuencia(java.lang.String x) {
        analisistexto.AnalisisTexto_Service service = new analisistexto.AnalisisTexto_Service();
        analisistexto.AnalisisTexto port = service.getAnalisisTextoPort();
        return port.mayorFrecuencia(x);
    }

    public static int numeroCaracteres(java.lang.String x) {
        analisistexto.AnalisisTexto_Service service = new analisistexto.AnalisisTexto_Service();
        analisistexto.AnalisisTexto port = service.getAnalisisTextoPort();
        return port.numeroCaracteres(x);
    }
    
}
