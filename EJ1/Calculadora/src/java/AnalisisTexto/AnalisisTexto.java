/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnalisisTexto;

import java.util.HashMap;
import java.util.StringTokenizer;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author J.Guzmán
 */
@WebService(serviceName = "AnalisisTexto")
public class AnalisisTexto {

    /**
     * This is a sample web service operation
     *
     * @param x
     * @return
     */
    
    @WebMethod(operationName = "contar_palabras")
    public int contar_palabras(@WebParam(name = "x") String x) {
        StringTokenizer st = new StringTokenizer(x);
        return st.countTokens();
    }

    @WebMethod(operationName = "frecuencia_palabras")
    public int frecuencia_palabras(@WebParam(name = "x") String x, @WebParam(name = "y") String y) {
        int p = 0;
        StringTokenizer st = new StringTokenizer(x);
        while (st.hasMoreElements()) {
            String token = st.nextToken();
            if (token.matches(y)) {
                p++;
            }
        }
        return p;
    }

    @WebMethod(operationName = "numero_caracteres")
    public int numero_caracteres(@WebParam(name = "x") String x) {
        int p = 0;
        char[] arr = x.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            p++; //conotando espacios como caracteres
        }
        return p;
    }

    @WebMethod(operationName = "mayor_frecuencia")
    public String mayor_frecuencia(@WebParam(name = "x") String x) {
        int p = 0, maxN = 0;
        String max = null;
        StringTokenizer st = new StringTokenizer(x);
        HashMap<String, Integer> hash = new HashMap<>();
        while (st.hasMoreElements()) {
            String token = st.nextToken();
            if (hash.containsKey(token)) {
                p = hash.get(token);
                hash.put(token, p + 1);
            } else {
                hash.put(token, 1);
                p = 0;
            }
            if (p + 1 > maxN) {
                maxN = p+1;
                max = token;
            }
        }
        return max;
    }
}
