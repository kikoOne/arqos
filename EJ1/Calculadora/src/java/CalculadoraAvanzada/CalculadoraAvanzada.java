/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CalculadoraAvanzada;

import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author J.Guzmán
 */
@WebService(serviceName = "CalculadoraAvanzada")
public class CalculadoraAvanzada {

    /**
     * This is a sample web service operation
     * @param x
     * @return 
     */
        @WebMethod(operationName = "maximo")
    public float maximo(@WebParam(name = "x") ArrayList<Float> x) {
        Float p = x.get(0);
        for (int i = 1; i < x.size(); i++) {
            if (x.get(i)> p) {
                p = x.get(i);
            }
        }
        return p;
    }
    
    @WebMethod(operationName = "media")
    public float media(@WebParam(name = "x") ArrayList<Float> x) {
        float p = 0;
            for (Float x1 : x) {
                p += x1;
            }
        return p/x.size();
    }
    
    @WebMethod(operationName = "minimo")
    public float minimo(@WebParam(name = "x") ArrayList<Float> x) {
        Float p = x.get(0);
        for (int i = 1; i < x.size(); i++) {
            if (x.get(i)< p) {
                p = x.get(i);
            }
        }
        return p;
    }
}
