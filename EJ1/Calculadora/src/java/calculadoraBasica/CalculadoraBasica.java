/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoraBasica;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author J.Guzmán
 */
@WebService(serviceName = "CalculadoraBasica")
public class CalculadoraBasica {

    /**
     * Web service operation
     *
     * @param x
     * @param y
     * @return
     */
    @WebMethod(operationName = "suma")
    public float suma(@WebParam(name = "x") float x, @WebParam(name = "y") float y) {
        //TODO write your implementation code here:
        return x + y;
    }

    @WebMethod(operationName = "resta")
    public float resta(@WebParam(name = "x") float x, @WebParam(name = "y") float y) {
        //TODO write your implementation code here:
        return x - y;
    }

    @WebMethod(operationName = "multiplicacion")
    public float multiplicacion(@WebParam(name = "x") float x, @WebParam(name = "y") float y) {
        //TODO write your implementation code here:
        return x * y;
    }

    @WebMethod(operationName = "division")
    public float division(@WebParam(name = "x") float x, @WebParam(name = "y") float y) throws ErrorPorCero {
        if (y == 0) {
            ErrorPorCero err = new ErrorPorCero(new ErrorPorCeroBean("No se puede dividir por 0"));
            throw err;
        }
        return x / y;
    }
}
