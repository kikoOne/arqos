/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculadoraBasica;

import javax.xml.ws.WebFault;

/**
 *
 * @author J.Guzmán
 */
@WebFault(name="ErrorPorCero")
public class ErrorPorCero extends Exception{
    private ErrorPorCeroBean errorBean = null;

    public ErrorPorCero(ErrorPorCeroBean errorBean) {
        super(errorBean.getMessage());
        this.errorBean = errorBean;
    }

    public ErrorPorCero(ErrorPorCeroBean errorBean, String message, Throwable cause) {
        super(message, cause);
        this.errorBean = errorBean;
    }

    public ErrorPorCero(String message) {
        super(message);
    }
    
    public ErrorPorCeroBean getErrorBean() {
        return errorBean;
    }
}
