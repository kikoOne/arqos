/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculadoraBasica;

/**
 *
 * @author J.Guzmán
 */
public class ErrorPorCeroBean {
    private String message;

    public ErrorPorCeroBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
