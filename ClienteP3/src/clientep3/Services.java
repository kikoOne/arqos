/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clientep3;

import java.util.List;

/**
 *
 * @author centolo
 */
public class Services {

    private String msg;

    public Services() {
    }

    public List<Cd> callService(String user, String password, String currency, String type, boolean sendEmail) {
        javax.xml.ws.Holder<java.lang.String> result = new javax.xml.ws.Holder<java.lang.String>();
        javax.xml.ws.Holder<clientep3.ConvertCurrencyResponse> products = new javax.xml.ws.Holder<clientep3.ConvertCurrencyResponse>();
        shopWSDLOperation(user, password, currency, type, sendEmail, result, products);
        this.msg = result.value;
        return products.value._return;
    }

    public static void shopWSDLOperation(java.lang.String user, java.lang.String password, java.lang.String currency, java.lang.String type, boolean sendEmail, javax.xml.ws.Holder<java.lang.String> result, javax.xml.ws.Holder<clientep3.ConvertCurrencyResponse> products) {
        clientep3.ShopWSDLService service = new clientep3.ShopWSDLService();
        clientep3.ShopWSDLPortType port = service.getShopWSDLPort();
        port.shopWSDLOperation(user, password, currency, type, sendEmail, result, products);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
