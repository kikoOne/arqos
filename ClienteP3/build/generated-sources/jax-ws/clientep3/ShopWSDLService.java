
package clientep3;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ShopWSDLService", targetNamespace = "http://j2ee.netbeans.org/wsdl/ServP3/ShopWSDL", wsdlLocation = "http://localhost:9080/ShopWSDLService/ShopWSDLPort?WSDL")
public class ShopWSDLService
    extends Service
{

    private final static URL SHOPWSDLSERVICE_WSDL_LOCATION;
    private final static WebServiceException SHOPWSDLSERVICE_EXCEPTION;
    private final static QName SHOPWSDLSERVICE_QNAME = new QName("http://j2ee.netbeans.org/wsdl/ServP3/ShopWSDL", "ShopWSDLService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:9080/ShopWSDLService/ShopWSDLPort?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SHOPWSDLSERVICE_WSDL_LOCATION = url;
        SHOPWSDLSERVICE_EXCEPTION = e;
    }

    public ShopWSDLService() {
        super(__getWsdlLocation(), SHOPWSDLSERVICE_QNAME);
    }

    public ShopWSDLService(WebServiceFeature... features) {
        super(__getWsdlLocation(), SHOPWSDLSERVICE_QNAME, features);
    }

    public ShopWSDLService(URL wsdlLocation) {
        super(wsdlLocation, SHOPWSDLSERVICE_QNAME);
    }

    public ShopWSDLService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SHOPWSDLSERVICE_QNAME, features);
    }

    public ShopWSDLService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ShopWSDLService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns ShopWSDLPortType
     */
    @WebEndpoint(name = "ShopWSDLPort")
    public ShopWSDLPortType getShopWSDLPort() {
        return super.getPort(new QName("http://j2ee.netbeans.org/wsdl/ServP3/ShopWSDL", "ShopWSDLPort"), ShopWSDLPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ShopWSDLPortType
     */
    @WebEndpoint(name = "ShopWSDLPort")
    public ShopWSDLPortType getShopWSDLPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://j2ee.netbeans.org/wsdl/ServP3/ShopWSDL", "ShopWSDLPort"), ShopWSDLPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SHOPWSDLSERVICE_EXCEPTION!= null) {
            throw SHOPWSDLSERVICE_EXCEPTION;
        }
        return SHOPWSDLSERVICE_WSDL_LOCATION;
    }

}
