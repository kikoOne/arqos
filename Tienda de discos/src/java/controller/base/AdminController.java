package controller.base;

import controller.helper.AdminLoginHelper;
import controller.helper.CatalogHelper;
import controller.helper.ManageUserHelper;
import controller.helper.NewProductHelper;
import controller.helper.ValorationHelper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdminController extends HttpServlet {

    private AdminLoginHelper logh;
    private CatalogHelper catalogh;
    private ValorationHelper valorationh;
    private NewProductHelper newProducth;
    private ManageUserHelper manageUserh;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void gotoPage(String page, HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher(page).forward(request, response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(controller.base.CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        request.setAttribute("server", "true");
        String location = request.getParameter("location");
        System.out.println(location);
        if (location == null) {
            catalogh = new CatalogHelper(request);
            short out = catalogh.getCatalogAdmin();
            if (out == 1) {
                gotoPage("admin/CatalogView.jsp", request, response);
            } else if (out == -1) {
                gotoPage("admin/LoginAdminView.jsp", request, response);
            } else if (out == 0) {
                gotoPage("admin/ErrorView.jsp", request, response);
            }
        } else {
            if (request.getSession().getAttribute("admin") != null || location.matches("login")) {
                switch (location) {
                    case "login":
                        logh = new AdminLoginHelper(request);
                        if (logh.login()) {
                            catalogh = new CatalogHelper(request);
                            if (catalogh.getCatalog()) {
                                gotoPage("admin/CatalogView.jsp", request, response);
                            } else {
                                gotoPage("admin/ErrorView.jsp", request, response);
                            }
                        } else {
                            gotoPage("admin/ErrorView.jsp", request, response);
                        }
                        break;
                    case "gotoUsers":
                        manageUserh = new ManageUserHelper(request);
                        if (manageUserh.getUsers()) {
                            gotoPage("admin/UsersView.jsp", request, response);
                        } else {
                            gotoPage("admin/ErrorView.jsp", request, response);
                        }
                        break;
                    case "gotoPasswordChange":
                        gotoPage("admin/PasswordChangeView.jsp", request, response);
                        break;
                    case "passwordChange":
                        manageUserh = new ManageUserHelper(request);
                        if (manageUserh.changePassword()) {
                            gotoPage("admin/SuccessView.jsp", request, response);
                        } else {
                            gotoPage("admin/ErrorView.jsp", request, response);
                        }
                        break;
                    case "removeUser":
                        manageUserh = new ManageUserHelper(request);
                        if (manageUserh.removeUser()) {
                            gotoPage("admin/SuccessView.jsp", request, response);
                        } else {
                            gotoPage("admin/ErrorView.jsp", request, response);
                        }
                        break;
                    case "gotoNewProduct":
                        gotoPage("admin/NewProductView.jsp", request, response);
                        break;
                    case "gotoNewCd":
                        gotoPage("admin/NewCdView.jsp", request, response);
                        break;
                    case "addNewProduct":
                        newProducth = new NewProductHelper(request);
                        if (newProducth.insertProduct()) {
                            gotoPage("admin/SuccessView.jsp", request, response);
                        } else {
                            gotoPage("admin/ErrorView.jsp", request, response);
                        }
                        break;
                    case "gotoLogin":
                        gotoPage("admin/LoginAdminView.jsp", request, response);
                        break;
                    case "seeProduct":
                        valorationh = new ValorationHelper(request);
                        String result = valorationh.getProductData();
                        if (result != null) {
                            if (result.equals("cd")) {
                                gotoPage("admin/CdView.jsp", request, response);
                            }
                        } else {
                            gotoPage("admin/ErrorView.jsp", request, response);
                        }
                        break;
                    case "logout":
                        logh = new AdminLoginHelper(request);
                        logh.logout();
                        gotoPage("admin/LoginAdminView.jsp", request, response);
                        break;
                }
            } else {
                gotoPage("admin/LoginAdminView.jsp", request, response);
            }
        }
    }
}
