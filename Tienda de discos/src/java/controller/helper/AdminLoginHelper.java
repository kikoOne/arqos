package controller.helper;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import model.dao.AdminUserDAO;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.vo.AdminUser;

public class AdminLoginHelper {

    private final HttpServletRequest request;
    private final AdminUserDAO uDAO;
    private final String email;
    private final String password;
    private AdminUser admin;

    public AdminLoginHelper(HttpServletRequest request) {
        this.email = request.getParameter("email");
        this.password = request.getParameter("password");
        this.request = request;

        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        uDAO = daoFactory.getAdminUserDAO();
    }

    public boolean logout() {
        if (request.getSession(false) != null) { // Si hay una sesión
            if (request.getSession(false).getAttribute("admin") != null) {
                request.getSession().setAttribute("admin", null);
                request.getSession().invalidate();
                return true;
            }
        }
        request.setAttribute("error", "No hay una sesión abierta.");
        return false;
    }

    public boolean login() {
        // Si ya hay una sesión vuelve al catálogo
        if (request.getSession(false) != null) { // Si hay una sesión
            if (request.getSession(false).getAttribute("admin") != null) {
                return true;
            }
        }

        try {
            admin = uDAO.getAdmin(email, password);
            if (admin != null) {
                request.getSession(true).setAttribute("admin", admin);
                return true;
            } else {
                request.setAttribute("error", "Usuario o contraseña incorrectos.");
                return false;
            }
        } catch (SQLException ex) {
            request.setAttribute("error", "Error en el acceso a la base de datos.");
            return false;
        }
    }
}
