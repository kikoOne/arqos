package controller.helper;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import model.dao.CustomerDAO;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.service.Cart;
import model.vo.Customer;

public class PurchaseHelper {

    private final HttpServletRequest request;
    private final ProductDAO productDao;
    private final CustomerDAO cDAO;
    private Customer customer;

    public PurchaseHelper(HttpServletRequest request) {
        this.request = request;
        DAOFactoryCreator creator = new DAOFactoryCreator();
        DAOFactory daoFactory = creator.getDAOFactory();
        productDao = daoFactory.getProductDAO();
        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        cDAO = daoFactory.getCustomerDAO();
    }

    public boolean buy() {
        Cart cart = (Cart) request.getSession().getAttribute("cart");
        String output;
        if (cart != null) {
            if (cart.getProducts().size() > 0) {
                try {
                    customer = (Customer) request.getSession().getAttribute("customer");
                    Float multiplier = (customer.getExpenditure() >= 100) ? 0.8F : 1F;
                    output = productDao.buy(cart, (String) request.getSession().getAttribute("shipAddress"), customer.getEmail(), multiplier);
                    if (output == null) {
                        customer = cDAO.getCustomer(customer.getEmail());
                        request.getSession().setAttribute("customer", customer);
                        return true;
                    } else {
                        request.getSession().setAttribute("error", output);
                        request.getSession().setAttribute("cart", null);
                        return false;
                    }
                } catch (SQLException ex) {
                    request.getSession().setAttribute("error", "Error en el acceso a la base de datos.");
                    return false;
                }
            }
        }
        request.getSession().setAttribute("error", "El carrito no puede estar vacío para comprar.");
        return false;
    }
}
