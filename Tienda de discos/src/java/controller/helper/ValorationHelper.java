package controller.helper;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import model.dao.CustomerDAO;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.dao.ValorationsDAO;
import model.service.ServicesClient;
import model.vo.Cd;
import model.vo.Customer;
import model.vo.Product;
import model.vo.Valoration;

public class ValorationHelper {

    private final HttpServletRequest request;
    private final ValorationsDAO daoValorations;
    private final ProductDAO daoProduct;
    private final CustomerDAO daoCustomer;

    public ValorationHelper(HttpServletRequest request) {

        this.request = request;
        DAOFactoryCreator creator = new DAOFactoryCreator();
        DAOFactory daoFactory = creator.getDAOFactory();
        daoValorations = daoFactory.getValorationsDAO();
        daoProduct = daoFactory.getProductDAO();
        daoCustomer = daoFactory.getCustomerDAO();
    }

    public String getProductData() {
        ArrayList<Valoration> valorations;
        Product p;
        try {
            p = daoProduct.getProduct(request.getParameter("id"));
        } catch (SQLException ex) {
            request.setAttribute("error", "Error al recuperar los datos del producto de la base de datos.");
            return null;
        }

        try {
            valorations = daoValorations.getValorations(request.getParameter("id"));
        } catch (SQLException ex) {
            request.setAttribute("error", "Error al recuperar los datos de las valoraciones de la base de datos.");
            return null;
        }

        if (p == null || valorations == null) {
            request.setAttribute("error", "Error en el proceso de recuperacion de datos.");
            return null;
        }

        if (p.getType().equals("cd")) {
            request.setAttribute("cd", (Cd) p);
            request.setAttribute("valorations", valorations);
            request.setAttribute("description_length", ServicesClient.numeroCaracteres(p.getDescription()));
            return "cd";
        } else {
            request.setAttribute("error", "El producto obtenido no esta contemplado.");
            return null;
        }
    }

    public boolean introduceValoration() {

        Customer customer = (Customer) request.getSession().getAttribute("customer");
        if (customer == null) {
            request.setAttribute("error", "Debes iniciar sesión para introducir una valoracion.");
            return false;
        }
        String id = request.getParameter("id");
        String score = request.getParameter("score");
        String comment = request.getParameter("comment");

        Integer iid;
        Integer iscore;
        try {
            iid = Integer.parseInt(id);
            iscore = Integer.parseInt(score);
        } catch (NumberFormatException ex) {
            request.setAttribute("error", "Error en el formato de los datos.");
            return false;
        }

        if (iid < 0 || iscore < 0 || iscore > 5 || comment == null || comment.equals("") || comment.length() > 1000) {
            request.setAttribute("error", "Error en el formato de los datos.");
            return false;
        }

        try {
            if (!daoCustomer.hasPurchasedProduct(customer.getEmail(), id)) {
                request.setAttribute("error", "Debes haber comprado este producto para dar una valoracion");
                return false;
            }
        } catch (SQLException ex) {
            request.setAttribute("error", "Error al comprobar si se puede introducir la valoracion.");
            return false;
        }
        Valoration valoration = new Valoration(id, customer.getEmail(), Integer.parseInt(score), comment);
        try {
            daoValorations.insertValoration(valoration);
        } catch (SQLException ex) {
            request.setAttribute("error", "No puedes valorar dos veces el mismo producto.");
            return false;
        }
        request.setAttribute("message", "Valoración añadida correctamente.");
        return true;
    }
}
