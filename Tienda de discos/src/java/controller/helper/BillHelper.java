package controller.helper;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.vo.Bill;
import model.vo.Customer;

public class BillHelper {

    private final HttpServletRequest request;
    private final ProductDAO pDAO;
    private Bill bill;

    public BillHelper(HttpServletRequest request) {
        this.request = request;
        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        pDAO = daoFactory.getProductDAO();
    }

    public boolean getBill() {
        if (logged()) {
            try {
                Customer customer = (Customer) request.getSession().getAttribute("customer");
                bill = pDAO.getLastBill(customer.getEmail());
                if (bill != null) {
                    request.setAttribute("bill", bill);
                    return true;
                } else {
                    request.setAttribute("error", "Error al acceder a la factura.");
                    return false;
                }
            } catch (SQLException ex) {
                request.setAttribute("error", "Error en el acceso a la base de datos.");
                return false;
            }
        }
        request.setAttribute("error", "Debe iniciar sesión para consultar la factura.");
        return false;
    }

    public Boolean logged() {
        if (this.request.getSession().getAttribute("customer") == null) {
            request.setAttribute("error", "Debes iniciar sesión para utilizar el carrito.");
            return false;
        }
        return true;
    }
}
