
package model.service;

import conv.ExceptionConversion_Exception;

/**
 *
 * @author jguzman
 */
public class ServicesClient {

    public static double convertCurrency(java.lang.String from, java.lang.String to) throws ExceptionConversion_Exception {
        conv.ConversorService_Service service = new conv.ConversorService_Service();
        conv.ConversorService port = service.getConversorServicePort();
        return port.convertCurrency(from, to);
    }

    public static int numeroCaracteres(java.lang.String x) {
        atext.AnalisisTexto_Service service = new atext.AnalisisTexto_Service();
        atext.AnalisisTexto port = service.getAnalisisTextoPort();
        return port.numeroCaracteres(x);
    }
}
