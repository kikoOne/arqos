/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.vo;

/**
 *
 * @author J.Guzmán
 */
public abstract class Product {

    protected String id;
    protected String title;
    protected Float price;
    protected String year;
    protected String type;
    protected Integer stock;
    protected Float vat;
    protected String description;

    public Product(String id, String title, String description, Float price, Float vat, String year, String type, Integer stock) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.vat = vat;
        this.year = year;
        this.type = type;
        this.stock = stock;
        this.description = description;
    }

    public Integer getStock() {
        return stock;
    }

    public Float getVat() {
        return vat;
    }

    public String getDescription() {
        return description;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Float getPrice() {
        return price;
    }

    public String getYear() {
        return year;
    }

    public void setVat(Float vat) {
        this.vat = vat;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

}
