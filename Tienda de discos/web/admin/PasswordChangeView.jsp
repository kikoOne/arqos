<%@include file="../frame/top.jsp"%>
<title>Acceso Administrador</title>
</head>
<body>
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Modificar contraseņa de ${param.email}</h1>
        </header>
        <br>
        <form id="form" class="pure-form" action="admin?location=passwordChange" method="post" accept-charset="UTF-8" autocomplete="off">
            Introduzca la nueva contraseņa
            <fieldset>
                <input required type="text" name="password" placeholder="Nueva contraseņa" maxlength="150" style="width: 300px">
            </fieldset>   
            <input required type="hidden" name="email" value="${param.email}">
            <fieldset>
                <button class="pure-button pure-button-primary"type="submit">Modificar</button>
            </fieldset>                 
        </form>
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>
