/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio1.model;

import java.io.Serializable;
import javax.ws.rs.FormParam;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author jguzman
 */
public class LoginUser {

    @FormParam("id")
    private String id;
    @FormParam("email")
    private String email;

    public LoginUser(String id, String email) {
        this.id = id;
        this.email = email;
    }

    @Override
    public String toString() {
        try {
            // takes advantage of toString() implementation to format {"a":"b"}
            return new JSONObject().put("id", id).put("email", email).toString();
        } catch (JSONException e) {
            return null;
        }
    }

    public LoginUser() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
