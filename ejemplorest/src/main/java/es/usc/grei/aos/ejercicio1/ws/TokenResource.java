package es.usc.grei.aos.ejercicio1.ws;

import es.usc.grei.aos.ejercicio1.model.LoginUser;
import es.usc.grei.aos.ejercicio1.model.MySQLConnection;
import es.usc.grei.aos.ejercicio1.model.User;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import io.jsonwebtoken.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

@Path("/token")
public class TokenResource {

    private final long TTL = 900000;

    @Context
    private ServletContext context;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createToken(LoginUser atempt) {
        try {
            if (login(atempt.getId(), atempt.getEmail())) {
                HashMap<String, Object> tokenmap = new HashMap<>();
                tokenmap.put("access_token", createJWT(atempt.getId(),  TTL));
                tokenmap.put("expires_in", TTL);
                return Response.ok(tokenmap).status(Response.Status.CREATED).build();
            }
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    private boolean login(String id, String email) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM usuarios WHERE id = " + id + " AND email = '" + email + "'");
            if (rs.next()) {
                con.close();
                return true;
            }
        }
        return false;
    }

    private String createJWT(String id, long time) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long now = System.currentTimeMillis();

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = "p1rest".getBytes();
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id).setIssuedAt(new Date(now)).signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (time >= 0) {
            Date exp = new Date(now + time);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }
}
