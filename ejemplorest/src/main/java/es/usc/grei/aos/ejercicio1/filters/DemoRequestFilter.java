package es.usc.grei.aos.ejercicio1.filters;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class DemoRequestFilter implements ContainerRequestFilter {

    private void parseJWT(String jwt) {
        Claims claims = Jwts.parser().setSigningKey("p1rest".getBytes()).parseClaimsJws(jwt).getBody();
        System.out.println("Nick: " + claims.getId());
        System.out.println("Expiration: " + claims.getExpiration());
    }

    @Override
    public void filter(ContainerRequestContext cr) throws IOException {
        System.out.println(cr.getUriInfo().getPath());
        if (!cr.getUriInfo().getPath().equals("token")) {
            try {
                parseJWT(cr.getHeaders().getFirst("x-auth-token"));
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
