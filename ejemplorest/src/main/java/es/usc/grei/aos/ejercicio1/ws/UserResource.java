package es.usc.grei.aos.ejercicio1.ws;

import es.usc.grei.aos.ejercicio1.model.MySQLConnection;
import es.usc.grei.aos.ejercicio1.model.User;
import es.usc.grei.aos.ejercicio1.model.WSResult;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author jguzman
 */
@Path("/")
public class UserResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserResource
     */
    public UserResource() {
    }

    @GET
    @Path("users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers() throws SQLException {
        ArrayList<User> users = new ArrayList<>();
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            User user = null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM usuarios ORDER BY id ASC");
            while (rs.next()) {
                user = new User(rs.getInt("id"), rs.getString("nombre"), rs.getString("apellidos"), rs.getString("telefono"), rs.getString("email"), rs.getString("direccion"));
                users.add(user);
            }
            con.close();
            return Response.ok(new WSResult(true, users)).build();
        }
    }

    @GET
    @Path("user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("id") String id) throws SQLException {

        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            User user = null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM usuarios WHERE id='" + id + "'");
            if (rs.next()) {
                user = new User(rs.getInt("id"), rs.getString("nombre"), rs.getString("apellidos"), rs.getString("telefono"), rs.getString("email"), rs.getString("direccion"));
                con.close();
                return Response.ok(new WSResult(true, user)).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Path("user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(User user) throws SQLException {
        //User user = null;
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Statement st = con.createStatement();
            st.executeUpdate("INSERT INTO usuarios (id, nombre, apellidos, telefono , email, direccion) VALUES (" + user.getId() + ", '" + user.getSurname() + "', '" + user.getName() + "', '" + user.getPhone() + "' ,'" + user.getEmail() + "', '" + user.getAddress() + "');");
            con.close();
            return Response.status(Response.Status.CREATED).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @PUT
    @Path("user/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyUser(User user, @PathParam("id") String id) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Statement st = con.createStatement();
            st.executeUpdate("UPDATE usuarios SET nombre = '" + user.getSurname() + "', apellidos = '" + user.getName() + "', telefono = '" + user.getPhone() + "', email = '" + user.getEmail() + "', direccion = '" + user.getAddress() + "' WHERE id = '" + id + "';");
            con.close();
            return Response.status(Response.Status.ACCEPTED).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE // Eliminar un usuario
    @Path("user/{id}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUsuario(@PathParam("id") String id) {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Statement st = con.createStatement();
            if (!st.execute("DELETE FROM usuarios WHERE id = " + id + "")) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            con.close();
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE // Eliminar un usuario
    @Path("user")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteAllUsuario() {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Statement st = con.createStatement();
            if (!st.execute("DELETE FROM usuarios")) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            con.close();
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (SQLException ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
