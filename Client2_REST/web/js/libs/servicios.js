var myapp = angular.module('MyApp', []);

myapp.controller('MyCtrl', ['$scope', '$http', '$location', '$window', '$routeParams', function ($scope, $http, $location, $window, $routeParams) {
        $scope.user = {};
        $scope.results = [];
        $scope.msg = "";
        $scope.getUsers = function () {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/Rest2/api/users',
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {
                $scope.results = response.data.content;
            }, function errorCallback(response) {
                if (response.status == 500) {
                    $window.location.href = 'LoginAdminView.jsp';
                }
            });
        };
        $scope.getCatalogue = function () {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/Rest2/api/products',
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {
                $scope.results = response.data.content;
            }, function errorCallback(response) {
                if (response.status == 500) {
                    $window.location.href = 'LoginAdminView.jsp';
                }
            });
        };

        $scope.getProduct = function () {
            $id = $location.search().id;
            //sconsole.log($id);
            $http({
                method: 'GET',
                url: ('http://localhost:8080/Rest2/api/product/' + $id),
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {
                $scope.productdata = response.data.content;
            }, function errorCallback(response) {
                if (response.status == 500) {
                    $window.location.href = 'LoginAdminView.jsp';
                }
            });
        };

        $scope.getProduct = function () {
            $id = $location.search().id;
            //sconsole.log($id);
            $http({
                method: 'GET',
                url: ('http://localhost:8080/Rest2/api/product/' + $id),
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {
                $scope.productdata = response.data.content;
            }, function errorCallback(response) {
                if (response.status == 500) {
                    $window.location.href = 'LoginAdminView.jsp';
                }
            });
        };

        $scope.searchProduct = function () {
            $http({
                method: 'GET',
                url: ('http://localhost:8080/Rest2/api/product/search/' + $scope.title + '/' + $scope.author + '/' + $scope.maxPrice + '/' + $scope.year),
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {
                $scope.results = response.data.content;
                //$window.location.reload();
            }, function errorCallback(response) {
                if (response.status == 500) {
                    $window.location.href = 'LoginAdminView.jsp';
                }
            });
            $scope.title = undefined;
            $scope.author = undefined;
            $scope.maxPrice = undefined;
            $scope.year = undefined;
        };

        $scope.deleteUser = function ($param) {
            console.log($param);
            $http({
                method: 'DELETE',
                url: ('http://localhost:8080/Rest2/api/user/' + $param),
                //contentType: "application/json",
                //data: dobj,
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {
                $window.location.reload();
            }, function errorCallback(response) {
                if (response.status == 500) {
                    $window.location.href = 'LoginAdminView.jsp';
                }
            });
        };
        $scope.deleteAllUsers = function () {
            $http({
                method: 'DELETE',
                url: 'http://localhost:8080/Rest2/api/user',
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {

            }, function errorCallback(response) {
                if (response.status == 500) {
                    $window.location.href = 'LoginAdminView.jsp';
                }
            });
        };
        $scope.newUser = function () {
            var dataObj = {
                name: $scope.name,
                shipAdress: $scope.shipAdress,
                email: $scope.email,
                password: $scope.password
            };
            $http({
                method: 'POST',
                url: 'http://localhost:8080/Rest2/api/user',
                contentType: "application/json",
                data: dataObj,
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {
                $window.location.href = 'UsersView.jsp';
            }, function errorCallback(response) {
                if (response.status == 500) {
                    $window.location.href = 'LoginAdminView.jsp';
                }
            });

            $scope.name = '';
            $scope.shipAdress = '';
            $scope.email = '';
            $scope.password = '';
        };

        $scope.updateUser = function () {
            var dataObj2 = {
                email: $location.search().email,
                password: $scope.password
            };

            $http({
                method: 'PUT',
                url: 'http://localhost:8080/Rest2/api/user',
                contentType: "application/json",
                data: dataObj2,
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {
                $window.location.href = 'UsersView.jsp';
            }, function errorCallback(response) {
                if (response.status == 500) {
                    $window.location.href = 'LoginAdminView.jsp';
                }
            });
            $scope.email = '';
            $scope.password = '';
        };

        $scope.login = function () {
            var dataLogin = {
                email: $scope.email,
                password: $scope.password
            };
            $http({
                method: 'POST',
                url: 'http://localhost:8080/Rest2/api/token',
                contentType: "application/json",
                data: dataLogin
            }).then(function successCallback(response) {
                var token = response.data.access_token;
                sessionStorage.token = token;
                $window.location.href = 'CatalogView.jsp';
                //$location.path('http://localhost:8080/Tienda_de_discos/store');
            }, function errorCallback(response) {

            });
        };

        $scope.logout = function () {
            sessionStorage.token = '';
            $window.location.href = 'LoginAdminView.jsp';
        };
    }]);