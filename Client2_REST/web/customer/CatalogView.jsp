<%@include file="../frame/top.jsp" %>
<title>Cat�logo</title>
</head>
<body>
    <%@include file="../frame/headerCustomer.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>${view}</h1>
        </header>
        <c:if test="${empty products}">
            No hay coincidencias o art�culos disponibles.
        </c:if>
        <table>
            <c:forEach var="product" items="${products}">
                <c:set var="boton" scope="page" value="A�adir al carrito"/>
                <c:set var="disabled" scope="page" value=""/>
                <c:if test="${product.stock == 0}">
                    <c:set var="disabled" scope="page" value="disabled"/>
                    <c:set var="boton" scope="page" value="No disponible"/>
                </c:if>

                <tr>
                    <td>
                        <a href="store?location=seeProduct&id=${product.id}">
                            <object data="./images/products/${product.id}.jpg" type="image/png" height="100">
                                <img src="./images/products/none.jpg" height="100">
                            </object>                       
                        </a>
                    </td>                
                    <td><a href="store?location=seeProduct&id=${product.id}">${product.title}</a></td>
                    <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${product.price * 1.21}"/>$</td>
                    <td>${product.year}</td>
                    <td>${product.stock} uds.</td>
                <form action="store?location=addCart" method="post" accept-charset="UTF-8">
                    <td class="pure-form"><input name="quantity" type="number" value="1" min="1" max="${product.stock}" ${disabled}></td>
                    <td><button name="product" value="${product.id}" class="pure-button pure-button-primary" type="submit" ${disabled}>${boton}</button></td>
                </form>
                </tr>
            </c:forEach>
        </table> 
    </section>     
</body>
<%@include file="../frame/bottomCustomer.jsp"%>