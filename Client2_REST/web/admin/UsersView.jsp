<%@include file="../frame/top.jsp" %>
<title>Cat�logo</title>
</head>
<body data-ng-init="getUsers()">
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Clientes</h1>
        </header>
        <table>
            <tr ng-repeat="customer in results">
                <td>{{customer.name}}</td>
                <td>{{customer.email}}</td>
                <td>
                    <button ng-click="deleteUser(customer.email);getUsers()" class="pure-button pure-button-primary">x</button>
                    <!--form id="form" class="pure-form" ng-submit="deleteUser()">
                        <fieldset>
                            <input type="text" ng-model="customer.email" ng-value="email" id="email" name="email" maxlength="150" style="width: 300px; display: block">
                            <input type="text" ng-model="customer.password" ng-value="password" id="password" name="password" maxlength="150" style="width: 300px; display: block">
                            <button class="pure-button pure-button-primary"type="submit">x</button>
                        </fieldset>                 
                    </form--></td>
                <td><a class="pure-button pure-button-primary" href="PasswordChangeView.jsp?email={{customer.email}}">Modificar contrase�a</a></td>
            </tr>
        </table>
        <button ng-click="deleteAllUsers()" class="pure-button pure-button-primary">Eliminar todos los usuarios</button>
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>