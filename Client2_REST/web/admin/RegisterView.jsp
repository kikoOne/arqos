<%@include file="../frame/top.jsp"%>
<title>Registro</title>
</head>
<body>
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Registro de usuarios</h1>
        </header>
        <form class="pure-form" role="form" ng-submit="newUser()">
            <fieldset>
                <input required type="text" name="name" ng-model="name" placeholder="Nombre y apellidos" maxlength="150" style="width: 300px">
            </fieldset>   
            <fieldset>
                <input required type="text" name="shipAddress" ng-model="shipAddress" placeholder="Direcci�n" maxlength="150" style="width: 300px">
            </fieldset>  
            <fieldset>
                <input required type="email" name="email" ng-model="email" placeholder="nombre@ejemplo.com" maxlength="100" style="width: 300px">
            </fieldset>        
            <fieldset>
                <input required type="password" name="password" ng-model="password" placeholder="&bull;&bull;&bull;&bull;" maxlength="100" style="width: 300px">
            </fieldset>             
            <button class="pure-button pure-button-primary"type="submit">Registrarse</button>
        </form>
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>