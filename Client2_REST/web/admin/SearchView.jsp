<%@include file="../frame/top.jsp" %>
<title>Cat�logo</title>
</head>
<body>
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>B�squeda de discos</h1>
        </header>
        <form class="pure-form" role="form" ng-submit="searchProduct()">
            <fieldset>
                <input name="title" type="text" placeholder="T�tulo" value="" ng-model="title">            
                <input name="author" type="text" placeholder="Autor" value="" ng-model="author">
            </fieldset>
            <fieldset>
                <input name="maxPrice" min="1" type="number" placeholder="Precio m�ximo" value="" ng-model="maxPrice">
                <input name="year" min="1900" max="2015" type="number" placeholder="A�o" value="" ng-model="year">
            </fieldset>
            <button class="pure-button pure-button-primary" type="submit">Buscar</button>
        </form>
        <table >
            <tr ng-repeat="product in results">
                <td>
                    <a href="CdView.jsp?id={{product.id}}">
                        <object data="../images/products/{{product.id}}.jpg" type="image/png" height="100">
                            <img src="../images/products/none.jpg" height="100">
                        </object>
                    </a>
                    <br>
                </td>                
                <td><a href="CdView.jsp?id={{product.id}}">{{product.title}}</a></td>
                <td>{{product.price * 1.21| number : 2}} $</td>
                <td>{{product.year}}</td>
                <td>{{product.stock}} uds.</td>
            </tr>
        </table> 
    </section>
</body>
<%@include file="../frame/bottomAdmin.jsp"%>