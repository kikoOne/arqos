<%@include file="../frame/top.jsp" %>
<title>${cd.title}</title>
</head>
<body data-ng-init="getProduct()">
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>{{productdata.product.title}}</h1>
        </header>

        <c:set var="boton" scope="page" value="A�adir al carrito"/>
        <c:set var="disabled" scope="page" value=""/>
        
        <img src="../images/products/{{productdata.product.id}}.jpg" height="200">
        
        <h3>Descripci�n</h3>
        <p>{{productdata.product.description}}</p>

        <h3>Autor: {{productdata.product.author}}</h3>
        <h3>A�o: {{productdata.product.year}}</h3>
        <h3>Pa�s: {{productdata.product.country}}</h3>
        <h3>Identificador: {{productdata.product.barCode}}</h3>


        <h3>Precio: {{productdata.product.price | number : 2}} $</h3>
        <br>
        Existen {{productdata.product.stock}} unidades disponibles.
        <br>
        <br>


        <c:choose>
            <c:when test="${empty productdata.valoration}">
                <p>No hay valoraciones para este art�culo</p>
            </c:when>
            <c:otherwise>
                <table>
                    <tr ng-repeat="valoration in productdata">          
                        <td>{{valoration.userEmail}}</td>
                        <td width="100">
                            <c:forEach var="i" begin="1" end="${valoration.score}">
                                <img src="../images/star.jpg" height="15">
                            </c:forEach>
                        </td>
                        <td>{{valoration.comment}}</td>
                    </tr>
                </table>
            </c:otherwise>
        </c:choose>
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>