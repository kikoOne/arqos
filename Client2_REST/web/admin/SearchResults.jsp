<%@include file="../frame/top.jsp" %>
<title>Cat�logo</title>
</head>
<body">
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>Cat�logo de discos</h1>
        </header>
        
        <table >
            <tr ng-repeat="product in results">
                <td>
                    <a href="CdView.jsp?id={{product.id}}">
                        <object data="../images/products/{{product.id}}.jpg" type="image/png" height="100">
                            <img src="../images/products/none.jpg" height="100">
                        </object>
                    </a>
                    <br>
                </td>                
                <td><a href="CdView.jsp?id={{product.id}}">{{product.title}}</a></td>
                <td>{{product.price * 1.21 | number : 2}} $</td>
                <td>{{product.year}}</td>
                <td>{{product.stock}} uds.</td>
            </tr>
        </table> 
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>