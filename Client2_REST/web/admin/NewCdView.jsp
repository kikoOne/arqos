<%@include file="../frame/top.jsp"%>
<title>Acceso Administrador</title>
</head>
<body>
    <%@include file="../frame/headerAdmin.jsp"%>
    <section id="zonaCentral">
        <header>
            <h1>A�adir CD</h1>
        </header>
        <br>
        <form id="form" class="pure-form" action="admin?location=addNewProduct" method="post" accept-charset="UTF-8" autocomplete="off">
            <fieldset>
                <input required type="text" name="title" placeholder="T�tulo" maxlength="150" style="width: 300px">
            </fieldset>   
            <fieldset>
                <input required type="text" name="author" placeholder="Autor" maxlength="150" style="width: 300px">
            </fieldset>  
            <fieldset>
                <input required type="text" name="country" placeholder="Pa�s" maxlength="100" style="width: 300px">
            </fieldset>        
            <fieldset>
                <input required type="number" name="barcode" placeholder="C�digo de barras" min="0" maxlength="100" style="width: 300px">
            </fieldset>     
            <fieldset>
                <input required type="number" name="year" placeholder="A�o" max="2015" maxlength="100" style="width: 300px">
            </fieldset>     
            <fieldset>
                <input required type="number" name="price" step="0.01" placeholder="Precio" min="0" maxlength="100" style="width: 300px">
            </fieldset>     
            <fieldset>
                <input required type="number" name="stock" placeholder="Cantidad" min="0" step="any" style="width: 300px">
            </fieldset>   
            Descripci�n:
            <fieldset>
                <textarea rows="5" cols="35" form="form" name="description"></textarea>
            </fieldset>                 
            <input required type="hidden" name="type" value="cd">
            <fieldset>
                <button class="pure-button pure-button-primary"type="submit">A�adir</button>
            </fieldset>                 
        </form>
    </section>     
</body>
<%@include file="../frame/bottomAdmin.jsp"%>
