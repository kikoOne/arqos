<header>
    <a href="store"><img id="logo" src="images/logo.png" height="50" alt="Logo"></a>
    <nav id="navp">
        <a href=".">Cat�logo</a>
        <a href="store?location=gotoSearch">Buscar</a>    
        <a href="store?location=showCart">Carrito</a>    
        <c:if test="${empty customer}"> 
            <a href="store?location=gotoRegister">Registro</a>
        </c:if>    
        <c:choose>
            <c:when test="${empty customer}"> 
                <a id="ultimoElemento" href="store?location=gotoLogin">Iniciar sesi�n</a>
            </c:when>
            <c:otherwise>   
                <a id="ultimoElemento" href="store?location=logout">Cerrar sesi�n</a>
            </c:otherwise>
        </c:choose>
    </nav>      
</header>
