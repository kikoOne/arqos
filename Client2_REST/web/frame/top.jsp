<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page isELIgnored="false"%>
<%--c:if test="${empty server}">
    <jsp:forward page="/forbidden.jsp"/>
</c:if--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang = "es" ng-app="MyApp" ng-controller="MyCtrl">
    <head>
        <meta charset="UTF-8">
        <link href="../css/pure-min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/estilos.css" rel="stylesheet" type="text/css"/>
        <script src="../js/libs/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="../js/libs/angular.js/angular.js" type="text/javascript"></script>
        <script src="../js/libs/scroll.js" type="text/javascript"></script>
        <script src="../js/libs/servicios.js" type="text/javascript"></script>