/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author J.Guzmán
 */
public class Customer extends User {

    private final Float expenditure;
    private final String address;

    public Customer(Float expenditure, String address, String name, String email, String password) {
        super(name, email, password);
        this.expenditure = expenditure;
        this.address = address;
    }

    public Float getExpenditure() {
        return expenditure;
    }

    public String getAddress() {
        return address;
    }
}
