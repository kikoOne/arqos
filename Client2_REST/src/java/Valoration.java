

public class Valoration {

    private final String productId;
    private final String userEmail;
    private final Integer score;
    private final String comment;

    public Valoration(String productId, String userEmail, Integer score, String comment) {
        this.userEmail = userEmail;
        this.productId = productId;
        this.score = score;
        this.comment = comment;
    }

    public String getProductId() {
        return productId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public Integer getScore() {
        return score;
    }

    public String getComment() {
        return comment;
    }
}
