/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author J.Guzmán
 */
public class Cd extends Product {

    private final String author;
    private final String country;
    private final String barCode;

    public Cd(String author, String country, String barCode, String id, String title, String description, Float price, Float vat, String year, String type, Integer stock) {
        super(id, title, description, price, vat, year, type, stock);
        this.author = author;
        this.country = country;
        this.barCode = barCode;
    }

    public String getAuthor() {
        return author;
    }

    public String getBarCode() {
        return barCode;
    }

    public String getCountry() {
        return country;
    }

    public String getCode() {
        return barCode;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
