
var myapp = angular.module('MyApp', []);
myapp.controller('MyCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.user = {};
        $scope.results = [];
        $scope.msg = "";
        $scope.getUsers = function () {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/ejemplorest/api/v1/users',
                headers: {
                    'x-auth-token': sessionStorage.token
                }
            }).then(function successCallback(response) {
                $scope.results = response.data.content;
            }, function errorCallback(response) {
                console.log("failed", response.toString());
            });
        };
        $scope.getUser = function () {
            $http({
                method: 'GET',
                url: ('http://localhost:8080/ejemplorest/api/v1/user/' + id.value)
            }).then(function successCallback(response) {
                $scope.user = response.data.content;
            }, function errorCallback(response) {
                console.log("failed", response.toString());
            });
        };
        $scope.deleteUser = function () {
            $http({
                method: 'DELETE',
                url: ('http://localhost:8080/ejemplorest/api/v1/user/' + id2.value)
            }).then(function successCallback(response) {

            }, function errorCallback(response) {
                console.log("failed", response.toString());
            });
        };
        $scope.deleteAllUsers = function () {
            $http({
                method: 'DELETE',
                url: 'http://localhost:8080/ejemplorest/api/v1/user'
            }).then(function successCallback(response) {

            }, function errorCallback(response) {
                console.log("failed", response.toString());
            });
        };
        $scope.newUser = function () {
            var dataObj = {
                id: $scope.id,
                surname: $scope.surname,
                name: $scope.name,
                phone: $scope.phone,
                email: $scope.email,
                address: $scope.address
            };
            $http({
                method: 'POST',
                url: 'http://localhost:8080/ejemplorest/api/v1/user',
                contentType: "application/json",
                data: dataObj
            }).then(function successCallback(response) {

            }, function errorCallback(response) {
                console.log("failed", response.toString());
            });
            $scope.id = '';
            $scope.surname = '';
            $scope.name = '';
            $scope.phone = '';
            $scope.email = '';
            $scope.address = '';
        };
        $scope.updateUser = function () {
            var dataObj2 = {
                id: $scope.id,
                surname: $scope.surname,
                name: $scope.name,
                phone: $scope.phone,
                email: $scope.email,
                address: $scope.address
            };
            $http({
                method: 'PUT',
                url: ('http://localhost:8080/ejemplorest/api/v1/user/' + id3.value),
                contentType: "application/json",
                data: dataObj2
            }).then(function successCallback(response) {

            }, function errorCallback(response) {
                console.log("failed", response.toString());
            });
            $scope.id = '';
            $scope.surname = '';
            $scope.name = '';
            $scope.phone = '';
            $scope.email = '';
            $scope.address = '';
        };
        $scope.login = function () {
            var dataLogin = {
                id: $scope.id,
                email: $scope.email
            };
            $http({
                method: 'POST',
                url: 'http://localhost:8080/ejemplorest/api/v1/token',
                contentType: "application/json",
                data: dataLogin
            }).then(function successCallback(response) {
                var token = response.data.access_token;
                sessionStorage.token = token;
            }, function errorCallback(response) {
                console.log("failed", response.toString());
            });
        };
    }]);