/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.service;

import java.util.HashMap;
import model.vo.CartProduct;
import model.vo.Product;

/**
 *
 * @author J.Guzmán
 */
public class Cart {

    private final HashMap<String, CartProduct> products;
    private Float total;
    private Float totalVat;

    public Cart() {
        products = new HashMap<>();
        total = 0F;
        totalVat = 0F;
    }

    public Float getTotalVat() {
        return totalVat;
    }

    public HashMap<String, CartProduct> getProducts() {
        return products;
    }

    public Float getTotal() {
        return total;
    }

    public void addProduct(Product product, Integer quantity) {
        CartProduct cp = new CartProduct(product, quantity);
        this.products.put(product.getId(), cp);
        recalcBill();
    }

    public void recalcBill() {
        this.total = 0F;
        this.totalVat = 0F;
        for (String key : this.products.keySet()) {
            this.total += this.products.get(key).getProduct().getPrice() * this.products.get(key).getQuantity();
            this.totalVat += this.products.get(key).getProduct().getVat() * this.products.get(key).getProduct().getPrice() * this.products.get(key).getQuantity();
        }
    }

    public void removeProduct(String id) {
        if (this.products.containsKey(id)) {
            this.products.remove(id);
            recalcBill();
        }
    }

}
