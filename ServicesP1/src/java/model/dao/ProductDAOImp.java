package model.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import model.service.Cart;
import model.vo.Bill;
import model.vo.CartProduct;
import model.vo.Cd;
import model.vo.Product;

public class ProductDAOImp implements ProductDAO {

    public ArrayList<Cd> getCdsList(HashMap<String, String> filter) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            ArrayList<Cd> cds;
            String[] wl = {"maxPrice", "title", "author", "year"};
            List<String> whiteList = Arrays.asList(wl);
            String filterLine = "";
            if (filter != null) {
                // Purgar los filtros recibidos no permitidos
                Iterator it = filter.entrySet().iterator();
                Map.Entry<String, String> cursor;
                while (it.hasNext()) {
                    cursor = (Map.Entry<String, String>) it.next();
                    if (!whiteList.contains(cursor.getKey())) {
                        it.remove();
                    }
                }

                // Generar línea de filtro para la sentencia sql con los filtros permitidos
                if (filter.size() > 0) {
                    filterLine = " WHERE ";
                }
                Integer counter = 0;
                for (String f : filter.keySet()) {
                    if (f.equals("maxPrice")) {
                        if (filter.get(f) != null && !filter.get(f).equals("")) {
                            filterLine += "price <= '" + Float.parseFloat(filter.get(f)) / getVat("cd") + "'";
                        } else {
                            filterLine += "price >= 0";
                        }
                    } else {
                        filterLine += f + " like '%" + filter.get(f) + "%'";
                    }
                    counter++;
                    if (counter != filter.size()) {
                        filterLine += " AND ";
                    }

                }
            }
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM cds" + filterLine);
            Float vat = getVat("cd");
            cds = new ArrayList();
            if (rs != null) {
                while (rs.next()) {
                    cds.add(new Cd(rs.getString("author"), rs.getString("country"), rs.getString("barcode"), rs.getString("id"), rs.getString("title"), rs.getString("description"), rs.getFloat("price"), vat, rs.getString("year"), "cd", rs.getInt("stock")));
                }
            }
            con.close();
            return cds;
        }
    }

    @Override
    public String buy(Cart cart, String shipAddress, String customerMail, Float multiplier) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            try {
                if (cart != null && !cart.getProducts().isEmpty()) {
                    con.setTransactionIsolation(4);
                    con.setAutoCommit(false);
                    Statement st = con.createStatement();

                    ResultSet rs;
                    boolean ok = true;
                    Integer stock = 0;
                    int i = 0;
                    ArrayList<Integer> stockProducts = new ArrayList<>();
                    Iterator it = cart.getProducts().values().iterator();
                    Float expenditure = 0.0F;

                    //comprobacion de stock
                    while (it.hasNext()) {
                        CartProduct prod = ((CartProduct) it.next());
                        if (prod.getProduct().getType().matches("cd")) {
                            rs = st.executeQuery("SELECT stock from cds WHERE id = '" + prod.getProduct().getId() + "' ");
                            if (rs.next()) {
                                stock = rs.getInt("stock");
                            }
                        }
                        //AL ESCALAR SE METEN LOS ELSE IF SEGUN LOS TYPE DE LOS PRODUCTOS
                        stockProducts.add(stock - prod.getQuantity());
                        if (stock < prod.getQuantity()) {
                            ok = false;//si se detecta stock insuficiente se pone ok a false y no se realizan mas consultas ==> se informa de ello
                        }
                    }
                    if (ok == true) {
                        //Consigo el expenditure del usuario para saber si hay descuento y para actualizarlo
                        rs = st.executeQuery("SELECT expenditure FROM customers WHERE email ='" + customerMail + "'");
                        if (rs.next()) {
                            expenditure = rs.getFloat("expenditure");
                        }

                        //Se inserta la compra segun si hay descuento o no
                        if (expenditure <= 100) {
                            st.executeUpdate("INSERT INTO purchases (date,shipaddress,total,totalvat,customer,discount) VALUES (CURDATE(),'" + shipAddress + "'," + cart.getTotal() + "," + cart.getTotalVat() * multiplier + ",'" + customerMail + "',0);");
                        } else {
                            st.executeUpdate("INSERT INTO purchases (date,shipaddress,total,totalvat,customer,discount) VALUES (CURDATE(),'" + shipAddress + "'," + cart.getTotal() * multiplier + "," + cart.getTotalVat() * multiplier + ",'" + customerMail + "',20);");
                        }

                        //Se obtiene el id de la purchase recien introducida
                        rs = st.executeQuery("select id from purchases order by id DESC LIMIT 1");
                        Integer purchase = null;
                        if (rs.next()) {
                            purchase = rs.getInt("id");
                        }
                        PreparedStatement pstmt;
                        String insert = "INSERT INTO orderLines (id,purchase,partial,vat,quantity) VALUES (?, ?, ?, ?, ?)";
                        pstmt = con.prepareStatement(insert);
                        for (String s : cart.getProducts().keySet()) {
                            pstmt.setInt(1, Integer.parseInt(cart.getProducts().get(s).getProduct().getId()));
                            pstmt.setInt(2, purchase);
                            pstmt.setFloat(3, cart.getProducts().get(s).getProduct().getPrice() * cart.getProducts().get(s).getQuantity() * multiplier);
                            pstmt.setFloat(4, getVat(cart.getProducts().get(s).getProduct().getType()));
                            pstmt.setInt(5, cart.getProducts().get(s).getQuantity());
                            pstmt.addBatch();
                        }
                        pstmt.executeBatch();

                        //Se modifica el expenditure
                        st.executeUpdate("UPDATE customers SET expenditure = " + (expenditure + cart.getTotalVat() * multiplier) + " WHERE email ='" + customerMail + "'");

                        //modifico el stock
                        it = cart.getProducts().values().iterator();
                        while (it.hasNext()) {
                            CartProduct prod = (CartProduct) it.next();
                            if (prod.getProduct().getType().matches("cd")) {
                                st.executeUpdate("UPDATE cds SET stock = '" + stockProducts.get(i) + "' WHERE id = '" + prod.getProduct().getId() + "' ");
                            }
                            //AL ESCALAR SE METEN LOS ELSE IF SEGUN LOS TYPE DE LOS PRODUCTOS
                            i++;
                        }
                        con.commit();
                        con.close();
                        return null;
                    } else {
                        return "No hay stock sufiente. Lo sentimos.";
                    }
                } else {
                    return "No hay elementos en el carrito";
                }
            } catch (SQLException ex) {
                con.rollback();
                con.close();
                throw new SQLException();
            }
        }
    }

    private HashMap<String, String> makeHash(ArrayList<String> input, ArrayList<String> data) {
        HashMap<String, String> filter = new HashMap<>();
        if (input != null && data != null) {
            for (int i = 0; i < input.size(); i++) {
                filter.put(input.get(i), data.get(i));
            }
            return filter;
        } else {
            return null;
        }
    }

    @Override
    public ArrayList<Product> getList(ArrayList<String> input, ArrayList<String> data) throws SQLException {
        ArrayList<Product> productList = new ArrayList<>();
        HashMap<String, String> filter = makeHash(input, data);
        ArrayList<Cd> cdList = getCdsList(filter);
        /*
         Para cada tipo de producto, se envía el hash de
         filtros, se recogen los resultados coincidentes 
         para los filtros aplicables y se genera una lista
         con todos los productos compatibles con los filtros.
         */
        for (Cd cd : cdList) {
            productList.add(cd);
        }
        return productList;
    }

    @Override
    public Product getProduct(String id) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Product p;
            p = null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM products WHERE id=" + id + ";");
            if (rs.next()) {
                String type = rs.getString("type");
                if (type.equals("cd")) {
                    p = getCd(id);
                }
            }
            con.close();
            return p;
        }
    }

    @Override
    public Cd getCd(String id) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Cd cd = null;
            Float vat = getVat("cd");
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM cds WHERE id=" + id + ";");
            if (rs.next()) {
                cd = new Cd(rs.getString("author"), rs.getString("country"), rs.getString("barcode"), rs.getString("id"), rs.getString("title"), rs.getString("description"), rs.getFloat("price"), vat, rs.getString("year"), "cd", rs.getInt("stock"));
            }
            con.close();
            return cd;
        }
    }

    public Float getVat(String type) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT vat FROM vats WHERE type='" + type + "';");
            if (rs.next()) {
                return rs.getFloat("vat");
            }
            con.close();
            return 1.21F;
        }
    }

    @Override
    public String getType(String id) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            con.setTransactionIsolation(4);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT type FROM products WHERE id='" + id + "';");
            if (rs.next()) {
                return rs.getString("type");
            }
            con.close();
            return null;
        }
    }

    @Override
    public Bill getLastBill(String customer) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            try {
                con.setTransactionIsolation(4);
                Bill bill = new Bill();
                Integer id = 0, quantity;
                String address, prodType, idProd, name;
                Float total, totalvat, vat, parcial, discount;
                ArrayList<CartProduct> billP = new ArrayList<>();
                CartProduct cartP;
                Cd cd;
                if (customer != null) {
                    Statement st = con.createStatement();
                    con.setAutoCommit(false);
                    ResultSet rs = st.executeQuery("SELECT id,shipaddress,total,totalvat,discount FROM purchases WHERE customer = '" + customer + "' AND id=(SELECT MAX(id) FROM purchases)");
                    if (rs.next()) {
                        id = rs.getInt("id");
                        address = rs.getString("shipaddress");
                        total = rs.getFloat("total");
                        totalvat = rs.getFloat("totalvat");
                        discount = rs.getFloat("discount");
                        bill.setShipAddress(address);
                        bill.setTotalAmount(total);
                        bill.setTotalAmountVat(totalvat);
                        bill.setDiscount(discount);
                        bill.setBillNumber(id);
                    }
                    if (id != 0) {
                        rs = st.executeQuery("SELECT a.*, b.type FROM orderLines a, products b  WHERE  a.id = b.id AND a.purchase = " + id);
                        while (rs.next()) {
                            parcial = rs.getFloat("partial");
                            //parcialvat = rs.getFloat("a.parcialvat");
                            quantity = rs.getInt("quantity");
                            idProd = rs.getString("id");
                            prodType = rs.getString("type");
                            vat = rs.getFloat("vat");
                            if (prodType.matches("cd")) {
                                cd = new Cd(null, null, null, idProd, null, null, parcial, vat, null, prodType, null);
                                cartP = new CartProduct(cd, quantity);
                                billP.add(cartP);
                            }
                        }
                        for (CartProduct billP1 : billP) {//recorro el array de productos y consulto los de cada tipo
                            if (billP1.getProduct().getType() != null && billP1.getProduct().getId() != null) {
                                if (billP1.getProduct().getType().equals("cd")) {
                                    rs = st.executeQuery("SELECT * FROM cds WHERE id = " + billP1.getProduct().getId());
                                    if (rs.next()) {
                                        name = rs.getString("title");
                                        billP1.getProduct().setTitle(name);
                                    }
                                }
                            } else {
                                return null;
                            }
                        }
                        con.commit();
                        con.close();
                        bill.setProducts(billP);
                        return bill;
                    }
                    return null;
                }
                return null;
            } catch (SQLException ex) {
                con.close();
                throw new SQLException();
            }
        }
    }

    @Override
    public boolean addProduct(Product product) throws SQLException {
        try (Connection con = new MySQLConnection().getConnection()) {
            try {
                con.setTransactionIsolation(4);
                Statement st = con.createStatement();
                String id;
                if (product != null) {
                    String type = product.getType();
                    if (type != null) {
                        con.setAutoCommit(false);
                        st.executeUpdate("INSERT INTO products (type) VALUES ('" + type + "')");
                        ResultSet rs = st.executeQuery("SELECT MAX(id) FROM products");
                        if (rs.next()) {
                            id = rs.getString("MAX(id)");
                            if (type.matches("cd")) {
                                st.executeUpdate("INSERT INTO cds (id, barcode, title, author, country, description, price, stock, year) VALUES "
                                        + "(" + id + ", '" + ((Cd) product).getBarCode() + "', '" + product.getTitle() + "', '" + ((Cd) product).getAuthor() + "', '" + ((Cd) product).getCountry() + "'"
                                        + ", '" + product.getDescription() + "', '" + product.getPrice() + "', " + product.getStock() + ", '" + product.getYear() + "')");
                            }
                        }
                        con.commit();
                        con.close();
                    } else {
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }
            } catch (SQLException ex) {
                con.rollback();
                con.close();
                throw new SQLException();
            }
        }
    }
}
