/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.vo;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author J.Guzmán
 */
//@XmlRootElement
//@XmlType(propOrder={"email", "password","name", "address", "expenditure"})
public class Customer extends User {

    private Float expenditure;
    private String address;

    public Customer(Float expenditure, String address, String name, String email, String password) {
        super(name, email, password);
        this.expenditure = expenditure;
        this.address = address;
    }

    public Float getExpenditure() {
        return expenditure;
    }

    public String getAddress() {
        return address;
    }

    public void setExpenditure(Float expenditure) {
        this.expenditure = expenditure;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
