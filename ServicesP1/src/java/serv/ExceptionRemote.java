/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.ws.WebFault;

/**
 *
 * @author jguzman
 */
@WebFault(name = "ExceptionRemote")
public class ExceptionRemote extends Exception{

    private ExceptionRemoteBean errorBean = null;

    public ExceptionRemote(ExceptionRemoteBean errorBean) {
        super(errorBean.getMessage());
        this.errorBean = errorBean;
    }

}
