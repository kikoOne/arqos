package serv;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.dao.ProductDAO;
import model.vo.Cd;
import model.vo.Product;

/**
 *
 * @author jguzman
 */
@XmlTransient
@XmlSeeAlso({Product.class, Cd.class})
@WebService(serviceName = "Filter")
public class Filter {

    private final ProductDAO pDAO;

    public Filter() {
        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        pDAO = daoFactory.getProductDAO();
    }

    @WebMethod(operationName = "doFilter")
    public ArrayList<Cd> doFilter(@WebParam(name = "data") String author) throws ExceptionRemote {
        ArrayList<Product> list;
        try {
//            ArrayList<String> input;
//            if (!author.matches("*")) {
//                input = new ArrayList<>();
//                input.add("author");
//                data = new ArrayList<>();
//                data.add(author);
//            } else {
//                input = null;
//                data = null;
//            }

            ArrayList<String> input = new ArrayList<>();
            input.add("author");
            ArrayList<String> data = new ArrayList<>();
            data.add(author);
            list = pDAO.getList(input, data);
            ArrayList<Cd> cds = new ArrayList<>();
            for (Product list1 : list) {
                cds.add((Cd) list1);
            }
            return cds;
        } catch (SQLException ex) {
            ExceptionRemote er = new ExceptionRemote(new ExceptionRemoteBean("Error en acceso a la base de datos"));
            throw er;
        }
    }
}
