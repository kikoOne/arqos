/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import model.dao.CustomerDAO;
import model.dao.DAOFactory;
import model.dao.DAOFactoryCreator;
import model.vo.Customer;

/**
 *
 * @author jguzman
 */
@WebService(serviceName = "LoginService")
public class LoginService {

    /**
     * This is a sample web service operation
     *
     * @param email
     * @param password
     * @return
     * @throws serv.ExceptionRemote
     */
    @WebMethod(operationName = "login")
    public Customer login(@WebParam(name = "email") String email, @WebParam(name = "password") String password) throws ExceptionRemote {
        DAOFactoryCreator daoFactoryCreator = new DAOFactoryCreator();
        DAOFactory daoFactory = daoFactoryCreator.getDAOFactory();
        CustomerDAO cDAO = daoFactory.getCustomerDAO();
        try {
            Customer customer = cDAO.getCustomer(email, password);
            return customer;
        } catch (Exception ex) {
            ExceptionRemote err = new ExceptionRemote(new ExceptionRemoteBean("Error en login"));
            throw err;
        }
    }
}
